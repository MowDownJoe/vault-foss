package com.mowdowndevelopments.vault;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class BulkEraser extends AsyncTask<Long, Integer, Void> {

	Context c;
	boolean isAccounts;
	ProgressDialog dialog;
	
	/**
	 * @param c
	 * @param isAccounts - If false, will attempt to delete transactions instead
	 */
	public BulkEraser(Context c, boolean isAccounts) {
		this.c = c;
		this.isAccounts = isAccounts;
	}

	@Override
	protected Void doInBackground(Long... params) {
		int length = params.length;
		for (int i = 0; i < length; i++) {
			if (isAccounts) {
				VaultDatabaseHelper.getInstance(c).deleteAccount(params[i]);
			} else {
				VaultDatabaseHelper.getInstance(c).deleteTransaction(params[i]);
			}
			publishProgress((int) ((i / (float) length) * 100));
		}
		dialog.dismiss();
		return null;
	}

	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onProgressUpdate(Progress[])
	 */
	@Override
	protected void onProgressUpdate(Integer... values) {
		if (dialog == null){
			dialog = ProgressDialog.show(c, "", c.getString(R.string.erasing), false, false);
		}
		dialog.setProgress(values[0]);
	}

}
