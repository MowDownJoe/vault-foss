package com.mowdowndevelopments.vault;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class TransactionRepetitionSimpleDialogFragment extends DialogFragment
		implements OnItemSelectedListener, OnClickListener,
		OnCheckedChangeListener {

	private OnRepetitionSet callbacks = null;
	Spinner timePeriodSelector;
	EditText numberOfPeriodsBox;
	CheckBox willRepeatBox, willNotifyBox;
	int spinnerIndex;
	public static long millisecondsInDay = 86400000;

	public interface OnRepetitionSet extends
			TransactionRepetitionDialogFragment.OnRepetitionSet {
		public void setRepetitionMode(int timePeriod, int howManyPeriods,
				boolean willNotify);
	}

	public TransactionRepetitionSimpleDialogFragment() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.DialogFragment#onAttach(android.app.Activity)
	 */
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		try {
			callbacks = (OnRepetitionSet) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ "must implement OnRepetitionSet.");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.DialogFragment#onDetach()
	 */
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		callbacks = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(
				R.layout.dialog_fragment_repeating_transaction_simple,
				container, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Fragment#onViewCreated(android.view.View,
	 * android.os.Bundle)
	 */
	@Override
	public void onViewCreated(View v, Bundle savedInstanceState) {
		super.onViewCreated(v, savedInstanceState);
		timePeriodSelector = (Spinner) v.findViewById(R.id.delayMultiplier);
		numberOfPeriodsBox = (EditText) v.findViewById(R.id.delayField);
		willRepeatBox = (CheckBox) v.findViewById(R.id.toRepeatBox);
		willNotifyBox = (CheckBox) v.findViewById(R.id.notifyOnRepeatBox);
		timePeriodSelector.setOnItemSelectedListener(this);
		willRepeatBox.setOnCheckedChangeListener(this);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			willNotifyBox.setVisibility(View.VISIBLE);
			timePeriodSelector.setVisibility(View.VISIBLE);
			numberOfPeriodsBox.setVisibility(View.VISIBLE);
			getView().findViewById(R.id.delayText).setVisibility(View.VISIBLE);
		} else {
			willNotifyBox.setVisibility(View.GONE);
			timePeriodSelector.setVisibility(View.GONE);
			numberOfPeriodsBox.setVisibility(View.GONE);
			getView().findViewById(R.id.delayText).setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cancelButton:
			dismiss();
			break;
		case R.id.okButton:
			String value = numberOfPeriodsBox.getText().toString().trim();
			if (value.contains(";") || value.contains("--")
					|| value.contains(")")) {
				Toast.makeText(getActivity(), R.string.noSpecialCharacters,
						Toast.LENGTH_LONG).show();
				break;
			}
			if (willRepeatBox.isChecked()) {
				try {
					callbacks.setRepetitionMode(spinnerIndex,
							Integer.parseInt(value), willNotifyBox.isChecked());
					dismiss();
				} catch (NumberFormatException e) {
					Toast.makeText(getActivity(), R.string.invalidBalArg,
							Toast.LENGTH_SHORT).show();
				}
			} else {
				callbacks.setRepetitionMode(0, 0, 0, false);
			}
			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> spinner, View v, int position,
			long id) {
		spinnerIndex = position;
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

}
