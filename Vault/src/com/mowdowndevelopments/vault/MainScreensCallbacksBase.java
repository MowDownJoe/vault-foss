package com.mowdowndevelopments.vault;

public interface MainScreensCallbacksBase
{
	public void setIfActionModeIsActive(boolean isActionModeActive);

	public boolean isActionModeActive();

	public boolean isDualPane();
}
