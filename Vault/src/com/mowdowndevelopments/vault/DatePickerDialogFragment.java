package com.mowdowndevelopments.vault;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

public class DatePickerDialogFragment extends DialogFragment implements
		OnDateSetListener {

	/**
	 * @author david
	 *
	 */
	public interface OnTransactionDateSetListener {
		public void onTransactionDateSet(int month, int day, int year);
	}

	private OnTransactionDateSetListener listener = null;
	private Calendar cal;
	private int month, day, year;
	protected static final String ARG_DAY = "com.mowdowndevelopments.vault.day", ARG_MONTH = "com.mowdowndevelopments.vault.month", ARG_YEAR = "com.mowdowndevelopments.vault.year", ARG_DATE = "com.mowdowndevelopments.vault.date";

	public DatePickerDialogFragment() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.DialogFragment#onCreateDialog(android.os.Bundle)
	 */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Bundle args = getArguments();
		if (args != null){
			cal = (Calendar) args.get(ARG_DATE);
		} else {
			cal = Calendar.getInstance();
		}
		month = cal.get(Calendar.MONTH);
		day = cal.get(Calendar.DAY_OF_MONTH);
		year = cal.get(Calendar.YEAR);
		getDialog().setTitle(R.string.DatePickerTitle);
		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Fragment#onViewCreated(android.view.View,
	 * android.os.Bundle)
	 */
	

	/* (non-Javadoc)
	 * @see android.app.DialogFragment#onAttach(android.app.Activity)
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			listener = (OnTransactionDateSetListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnTransactionDateSetListener.");
		}
	}

	/* (non-Javadoc)
	 * @see android.app.DialogFragment#onDetach()
	 */
	@Override
	public void onDetach() {
		super.onDetach();
		listener = null;
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		listener.onTransactionDateSet(monthOfYear, dayOfMonth, year);
	}

}
