package com.mowdowndevelopments.vault;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

public class TransactionRepetitionDialogFragment extends DialogFragment
		implements OnCheckedChangeListener, OnClickListener, AdapterView.OnItemSelectedListener {

	int spinnerSelection=0;
	RadioGroup buttons;
	Spinner onXofYSpinner, everyXYSpinner;
	OnRepetitionSet callbacks = null;
	static final String ARG_TYPE = "type", ARG_TIME = "time", ARG_DELAY = "delay";
	
	public interface OnRepetitionSet {
		public void setRepetitionMode(int repeatMode, int delayType, int delayTime, boolean willNotify);
	}

	public TransactionRepetitionDialogFragment() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see android.app.DialogFragment#onAttach(android.app.Activity)
	 */
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		try {
			callbacks = (OnRepetitionSet) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + "must implement OnRepetitionSet.");
		}
	}

	/* (non-Javadoc)
	 * @see android.app.DialogFragment#onDetach()
	 */
	@Override
	public void onDetach() {
		super.onDetach();
		callbacks = null;
	}

	/* (non-Javadoc)
	 * @see android.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.dialog_fragment_repeating_transaction, container, false);
	}

	/* (non-Javadoc)
	 * @see android.app.Fragment#onViewCreated(android.view.View, android.os.Bundle)
	 */
	@Override
	public void onViewCreated(View v, Bundle savedInstanceState) {
		super.onViewCreated(v, savedInstanceState);
		v.findViewById(R.id.cancelButton).setOnClickListener(this);
		v.findViewById(R.id.okButton).setOnClickListener(this);
		buttons = (RadioGroup) v.findViewById(R.id.repeatRadioGroup);
		buttons.setOnCheckedChangeListener(this);
		onXofYSpinner = (Spinner) v.findViewById(R.id.onXofYSpinner);
		everyXYSpinner = (Spinner) v.findViewById(R.id.setYasTimeTypeSpinner);
		onXofYSpinner.setOnItemSelectedListener(this);
		everyXYSpinner.setOnItemSelectedListener(this);
		Bundle arguments = getArguments();
		if (arguments != null){
			//TODO
		}
		getDialog().setTitle(R.string.setRepeat);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){
		case R.id.okButton:
			//TODO
			String value = "";
			if (value.contains(";") || value.contains("--")
					|| value.contains(")")) {
				Toast.makeText(getActivity(), R.string.noSpecialCharacters,
						Toast.LENGTH_LONG).show();
				break;
			}
			dismiss();
			break;
		case R.id.cancelButton:
			dismiss();
			break;
		}
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		Log.d("Radio Group test", "Radio button clicked: "+checkedId);
	}

	@Override
	public void onItemSelected(AdapterView<?> spinner, View v, int position,
			long id) {
		spinnerSelection = position;
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}

}
