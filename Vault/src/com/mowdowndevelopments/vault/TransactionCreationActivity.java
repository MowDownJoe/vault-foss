package com.mowdowndevelopments.vault;

import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;
import android.app.DatePickerDialog;

public class TransactionCreationActivity extends Activity implements
		TagEntryDialogFragment.OnTagCreation,
		TransactionCreationFormFragment.Callbacks,
		DatePickerDialogFragment.OnTransactionDateSetListener,
		TransactionRepetitionSimpleDialogFragment.OnRepetitionSet,
		DatePickerDialog.OnDateSetListener {
	private long accountID, transactionID;
	private boolean wasDateSet;
	private int repeatMode = 0, delayType = 0, delayTime = 0;
	long delayMillis;
	private boolean willNotifyOnRepeat;
	final static String ARG_WITH_EXTRA_ID = "com.mowdowndevelopments.vault.has_extra_id";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transaction_entry_layout);
		accountID = getIntent().getLongExtra(
				AccountDetailFragment.ARG_ACCOUNT_ID, -1);
		transactionID = getIntent().getLongExtra(
				AccountDetailFragment.ARG_TRANS_ID, -1);
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = new MenuInflater(this);
		inflater.inflate(R.menu.activity_create_transaction, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			confirmExitWithoutCreation();
			return true;
		case (R.id.acceptTrans):
			saveTransaction();
			return true;
		case (R.id.confirmDiscardTrans):
			returnToParent();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void saveTransaction() {
		try {
			TagListFragment tagFragment = (TagListFragment) getFragmentManager()
					.findFragmentById(R.id.TagListFragment);
			TransactionCreationFormFragment formFragment = (TransactionCreationFormFragment) getFragmentManager()
					.findFragmentById(R.id.formFragment);
			if (transactionID == -1) {
				transactionID = VaultDatabaseHelper.getInstance(this)
						.newTransaction(accountID,
								formFragment.getPartyBoxInput(),
								formFragment.getCheckNoBoxValue(),
								formFragment.getDate().getTimeInMillis(),
								formFragment.getAmountBoxValue(),
								formFragment.getTransactionType());
				if (repeatMode != 0) {
					VaultDatabaseHelper.getInstance(this)
							.setTransactionRepetition(transactionID,
									willNotifyOnRepeat, repeatMode, delayTime,
									delayType, delayMillis);
				}
			} else {
				VaultDatabaseHelper.getInstance(this).updateTransaction(
						transactionID, formFragment.getPartyBoxInput(),
						formFragment.getCheckNoBoxValue(),
						formFragment.getDate().getTimeInMillis(),
						formFragment.getAmountBoxValue(),
						formFragment.getTransactionType());
				if (repeatMode != 0
						&& !VaultDatabaseHelper.getInstance(this)
								.doesTransactionRepeat(transactionID)) {
					VaultDatabaseHelper.getInstance(this)
							.setTransactionRepetition(transactionID,
									willNotifyOnRepeat, repeatMode, delayTime,
									delayType, delayMillis);
				} else if (repeatMode != 0
						&& VaultDatabaseHelper.getInstance(this)
								.doesTransactionRepeat(transactionID)) {
					VaultDatabaseHelper.getInstance(this)
							.editTransactionRepetition(transactionID,
									willNotifyOnRepeat, repeatMode, delayTime,
									delayType, delayMillis);
				} else if (repeatMode == 0 && VaultDatabaseHelper.getInstance(this).doesTransactionRepeat(transactionID)){
					VaultDatabaseHelper.getInstance(this).unsetTransactionRepetition(transactionID);
				}
				VaultDatabaseHelper.getInstance(this)
						.clearTagUsageForTransaction(transactionID);
			}

			tagFragment.associateTagsWithTransaction(transactionID);
			returnToParent();
		} catch (IllegalArgumentException e) {
			Toast.makeText(this, R.string.badTransactionValue,
					Toast.LENGTH_SHORT).show();
		} catch (ParseException e) {
			Toast.makeText(this, R.string.badTransactionValue,
					Toast.LENGTH_SHORT).show();
		}
	}

	public void setTransactionDate(View v) {
		Calendar cal;
		if (transactionID != -1 || wasDateSet) {
			cal = ((TransactionCreationFormFragment) getFragmentManager()
					.findFragmentById(R.id.formFragment)).getDate();
		} else {
			cal = Calendar.getInstance();
		}
		DatePickerDialog dialog = new DatePickerDialog(this, this,
				cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
				cal.get(Calendar.DAY_OF_MONTH));
		dialog.setTitle(R.string.DatePickerTitle);
		dialog.show();
	}

	public void setRepetition(View v) {
		TransactionRepetitionDialogFragment fragment = new TransactionRepetitionDialogFragment();
		fragment.show(getFragmentManager(), "repeat");
	}

	@Override
	public void addTag(String tag) {
		((TagListFragment) getFragmentManager().findFragmentById(
				R.id.TagListFragment)).addTag(tag);
	}

	public void returnToParent() {
		Intent intent;
		if (getIntent()
				.getBooleanExtra(AccountListActivity.ARG_DUAL_PANE, true)) {
			intent = new Intent(this, AccountListActivity.class).putExtra(
					ARG_WITH_EXTRA_ID, true);
		} else {
			intent = new Intent(this, AccountDetailActivity.class);
		}
		intent.putExtra(AccountDetailFragment.ARG_ACCOUNT_ID, accountID);
		NavUtils.navigateUpTo(this, intent);
	}

	public void confirmExitWithoutCreation() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.leaveWithUnsavedChanges).setPositiveButton(
				R.string.dialogYes, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						returnToParent();
					}
				});
		builder.setNegativeButton(R.string.dialogNo,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		builder.create().show();

	}

	/* (non-Javadoc)
	 * @see android.app.Activity#dispatchKeyEvent(android.view.KeyEvent)
	 */
	@Override
	public void onBackPressed() {
		confirmExitWithoutCreation();
	}

	@Override
	public long getTransactionID() {
		return transactionID;
	}

	@Override
	public void setTransactionID(long transID) {
		transactionID = transID;
	}

	@Override
	public long getAccountID() {
		return accountID;
	}

	@Override
	public void setAccountID(long acctID) {
		accountID = acctID;
	}

	@Override
	public void onTransactionDateSet(int month, int day, int year) {
		wasDateSet = true;
		((TransactionCreationFormFragment) getFragmentManager()
				.findFragmentById(R.id.formFragment)).setDate(month, day, year);
	}

	@Override
	public void onRepetitionTypeSet() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		onTransactionDateSet(monthOfYear, dayOfMonth, year);
	}

	@Override
	public void setRepetitionMode(int repeatMode, int delayType, int delayTime,
			boolean willNotify) {
		this.repeatMode = repeatMode;
		this.delayType = delayType;
		this.delayTime = delayTime;
		willNotifyOnRepeat = willNotify;
		if (repeatMode == 1) {
			GregorianCalendar gCal = new GregorianCalendar();
			switch (delayType) {
			case 0: // Days
				delayMillis = delayTime
						* TransactionRepetitionSimpleDialogFragment.millisecondsInDay;
				break;
			case 1: // Weeks
				delayMillis = delayTime
						* 7
						* TransactionRepetitionSimpleDialogFragment.millisecondsInDay;
				break;
			case 2: // Months
				gCal.roll(Calendar.MONTH, delayTime);
				delayMillis = gCal.getTimeInMillis()
						- Calendar.getInstance().getTimeInMillis();
				break;
			case 3: // Years
				gCal.roll(Calendar.YEAR, delayTime);
				delayMillis = gCal.getTimeInMillis()
						- Calendar.getInstance().getTimeInMillis();
				break;
			default:
				delayMillis = 0;
			}
			((TransactionCreationFormFragment) getFragmentManager()
					.findFragmentById(R.id.formFragment)).setRepeatButtonText(
					repeatMode, delayTime, delayType);
		} else {
			delayMillis = 0;
		}
	}

	@Override
	public void setRepetitionMode(int timePeriod, int howManyPeriods,
			boolean willNotify) {
		setRepetitionMode(1, timePeriod, howManyPeriods, willNotify);
	}

}
