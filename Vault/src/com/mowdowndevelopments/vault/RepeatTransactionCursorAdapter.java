package com.mowdowndevelopments.vault;

import java.text.NumberFormat;

import android.content.Context;
import android.database.Cursor;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

public class RepeatTransactionCursorAdapter extends ResourceCursorAdapter {

	final static String LOADEREX_INIT_QUERY = "SELECT " + "R."
			+ VaultDatabaseHelper.ID + " AS " + VaultDatabaseHelper.REALID
			+ ", " + "R." + VaultDatabaseHelper.REPEATTYPE + ", R."
			+ VaultDatabaseHelper.DELAY + ", R."
			+ VaultDatabaseHelper.DELAYTYPE + ", T."
			+ VaultDatabaseHelper.DATEINMILLIS + ", T."
			+ VaultDatabaseHelper.TRANSVALUE + ", P."
			+ VaultDatabaseHelper.PARTYNAME + ", A."
			+ VaultDatabaseHelper.ACCTNAME + " FROM "
			+ VaultDatabaseHelper.REPEATSTABLE + " AS R, "
			+ VaultDatabaseHelper.TRANSTABLE + " AS T, "
			+ VaultDatabaseHelper.PARTYTABLE + " AS P, "
			+ VaultDatabaseHelper.ACCTTABLE + " AS A WHERE R."
			+ "Transaction_ID" + " = T." + VaultDatabaseHelper.ID + " AND T."
			+ "Account_ID" + " = A." + VaultDatabaseHelper.ID + " AND T."
			+ "Party_ID" + " = P." + VaultDatabaseHelper.ID;
	int layout;

	public RepeatTransactionCursorAdapter(Context context, int layout,
			Cursor c, boolean autoRequery) {
		super(context, layout, c, autoRequery);
		// TODO Auto-generated constructor stub
	}

	public RepeatTransactionCursorAdapter(Context context, int layout,
			Cursor c, int flags) {
		super(context, layout, c, flags);
		// TODO Auto-generated constructor stub
	}

	public RepeatTransactionCursorAdapter(Context cxt, Cursor c) {
		super(cxt, R.layout.repeating_transaction_list_item, c, false);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View v = LayoutInflater.from(context).inflate(layout, parent, false);
		v.setTag(R.id.TransactionName, v.findViewById(R.id.TransactionName));
		v.setTag(R.id.valueText, v.findViewById(R.id.valueText));
		v.setTag(R.id.accountText, v.findViewById(R.id.accountText));
		v.setTag(R.id.originalDateText, v.findViewById(R.id.originalDateText));
		v.setTag(R.id.repeatFrequencyText,
				v.findViewById(R.id.repeatFrequencyText));
		return v;
	}

	@Override
	public void bindView(View v, Context cxt, Cursor c) {
		TextView valueText = (TextView) v.getTag(R.id.valueText);
		TextView accountText = (TextView) v.getTag(R.id.accountText);
		TextView transactionName = (TextView) v.getTag(R.id.TransactionName);
		TextView originalDate = (TextView) v.getTag(R.id.originalDateText);
		TextView frequencyText = (TextView) v.getTag(R.id.repeatFrequencyText);
		transactionName.setText(c.getString(c.getColumnIndexOrThrow("P."
				+ VaultDatabaseHelper.PARTYNAME)));
		originalDate.setText(DateUtils.formatDateTime(
				cxt,
				c.getLong(c.getColumnIndexOrThrow("T."
						+ VaultDatabaseHelper.DATEINMILLIS)),
				DateUtils.FORMAT_NUMERIC_DATE));
		accountText.setText(c.getString(c.getColumnIndexOrThrow("A."
				+ VaultDatabaseHelper.ACCTNAME)));
		valueText.setText(NumberFormat.getCurrencyInstance().format(
				Math.abs(c.getDouble(c.getColumnIndexOrThrow("T."
						+ VaultDatabaseHelper.TRANSVALUE)))));
		switch (c.getInt(c.getColumnIndexOrThrow("R."
				+ VaultDatabaseHelper.REPEATTYPE))) {
		case 1: //To be expanded on in the future!
			StringBuilder builder = new StringBuilder()
					.append(cxt.getString(R.string.every))
					.append(' ')
					.append(c.getInt(c.getColumnIndexOrThrow("R."
							+ VaultDatabaseHelper.DELAY))).append(' ');
			String[] timeFrames = cxt.getResources().getStringArray(
					R.array.timeframes);
			builder.append(timeFrames[c.getInt(c.getColumnIndexOrThrow("R."
					+ VaultDatabaseHelper.DELAYTYPE))]);
			frequencyText.setText(builder.toString());
		default:
			frequencyText.setText("ERROR");
		}
	}

}
