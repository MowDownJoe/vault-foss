package com.mowdowndevelopments.vault;

import java.util.Arrays;
import java.util.LinkedList;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

public class TagEntryDialogFragment extends DialogFragment implements
		OnClickListener {

	public interface OnTagCreation {
		public void addTag(String tag);
	}

	// private VaultDatabaseHelper helper;
	protected static final String ARG_OLDTAG = "com.mowdowndevelopments.vault.OldTag";
	LinkedList<String> list;
	ArrayAdapter<String> adapter;
	private AutoCompleteTextView tagBox;
	private OnTagCreation callbacks = null;

	public TagEntryDialogFragment() {
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callbacks = (OnTagCreation) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.okButton:
			String value = tagBox.getText().toString().trim();
			if (value.contains(";") || value.contains(")")
					|| value.contains("--") || value.contains("\"")) {
				Toast.makeText(getActivity(), R.string.noSpecialCharacters,
						Toast.LENGTH_LONG).show();
				break;
			}
			if (!value.isEmpty()) {
				callbacks.addTag(value);
			}
			dismiss();
			break;
		case R.id.cancelButton:
			dismiss();
			break;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.dialog_fragment_add_tag, container,
				false);

	}

	@Override
	public void onViewCreated(View v, Bundle savedInstanceState) {
		super.onViewCreated(v, savedInstanceState);
		Bundle arguments = getArguments();
		tagBox = (AutoCompleteTextView) v
				.findViewById(R.id.tagEntryAutoCompleteBox);
		if (arguments != null) {
			String oldTag = arguments.getString(ARG_OLDTAG, "");
			tagBox.setText(oldTag);
		}
		v.findViewById(R.id.okButton).setOnClickListener(this);
		v.findViewById(R.id.cancelButton).setOnClickListener(this);
		list = new LinkedList<String>();
		adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line,
				android.R.id.text1, list);
		tagBox.setAdapter(adapter);
		getDialog().setTitle(R.string.TagEntryTitle);
		getDialog().getWindow().setSoftInputMode(
				LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		new SuggestionBuilder(getActivity()).execute();
	}

	public class SuggestionBuilder extends TagListBuilderBase {

		public SuggestionBuilder(Context context) {
			super(context);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (!isRemoving() || !isDetached()) {
				list.addAll(Arrays.asList(tags));
				adapter.notifyDataSetChanged();
			}
		}

	}

}
