package com.mowdowndevelopments.vault;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/**
 * An activity representing a list of Accounts. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link AccountDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link AccountListFragment} and the item details (if present) is a
 * {@link AccountDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link AccountListFragment.Callbacks} interface to listen for item
 * selections.
 */
public class AccountListActivity extends Activity implements
		AccountListFragment.Callbacks, AccountDetailFragment.Callbacks,
		SearchResultsListFragment.Callbacks,
		NewAccountDialogFragment.OnAccountCreated, ModifyAccountDialogFragment.OnAccountEdited {

	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private boolean mTwoPane, mIsActionModeActive, mIsSearchDisplayed = false;
	private Long mID;
	protected static final String ARG_DUAL_PANE = "com.mowdowndevelopments.vault.is_dual_pane";
	int backStackID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		ActionBar actionBar;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account_list);
		actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(false);
		if (findViewById(R.id.account_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;

		} else {
			actionBar.setSubtitle(R.string.accountSelect);
		}

		// TODO: If exposing deep links into your app, handle intents here.
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (getIntent().getBooleanExtra(
				TransactionCreationActivity.ARG_WITH_EXTRA_ID, false)) {
			onItemSelected(getIntent().getLongExtra(
					AccountDetailFragment.ARG_ACCOUNT_ID, -1));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.generic_activity_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.settingsMenuItem:
			intent = new Intent(this, SettingsActivity.class).putExtra(
					SettingsActivity.ARG_SOURCE_ACTIVITY_NAME,
					this.getClass().getName());
			if (mID != null) {
				intent.putExtra(AccountDetailFragment.ARG_ACCOUNT_ID, mID)
						.putExtra(
								TransactionCreationActivity.ARG_WITH_EXTRA_ID,
								true);
			} else {
				intent.putExtra(TransactionCreationActivity.ARG_WITH_EXTRA_ID, false);
			}
			startActivity(intent);
			return true;
		case R.id.manageRepeats:
			intent = new Intent(this, RepeatingTransactionsActivity.class)
					.putExtra(SettingsActivity.ARG_SOURCE_ACTIVITY_NAME,
							this.getClass().getName());
			if (mID != null) {
				intent.putExtra(AccountDetailFragment.ARG_ACCOUNT_ID, mID)
						.putExtra(
								TransactionCreationActivity.ARG_WITH_EXTRA_ID,
								true);
			} else {
				intent.putExtra(TransactionCreationActivity.ARG_WITH_EXTRA_ID, false);
			}
			startActivity(intent);
			return true;
		case R.id.reportIssue:
			intent = new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://bitbucket.org/MowDownJoe/vault/issues"))
					.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			return true;
		case R.id.about:
			new AboutDialogFragment().show(getFragmentManager(), "AboutThis");
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Callback method from {@link AccountListFragment.Callbacks} indicating
	 * that the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(long id) {
		if (mTwoPane) {
			// In two-pane mode, show the detail view in this activity by
			// adding or replacing the detail fragment using a
			// fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putLong(AccountDetailFragment.ARG_ACCOUNT_ID, id);
			AccountDetailFragment fragment = new AccountDetailFragment();
			fragment.setArguments(arguments);
			getFragmentManager()
					.beginTransaction()
					.replace(R.id.account_detail_container, fragment,
							"Account " + id).commit();
			mID = id;
		} else {
			// In single-pane mode, simply start the detail activity
			// for the selected item ID.
			Intent detailIntent = new Intent(this, AccountDetailActivity.class);
			detailIntent.putExtra(AccountDetailFragment.ARG_ACCOUNT_ID, id);
			startActivity(detailIntent);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// VaultDatabaseHelper.getInstance(getApplication()).close();
	}

	public void onCheckboxClicked(View v) {
		if (mIsSearchDisplayed) {
			((SearchResultsListFragment) getFragmentManager()
					.findFragmentByTag("Account " + mID)).onCheckboxClicked(v);
		} else {
			((AccountDetailFragment) getFragmentManager().findFragmentByTag(
					"Account " + mID)).onCheckboxClicked(v);
		}
	}

	public boolean isDualPane() {
		return mTwoPane;
	}

	@Override
	public void setIfActionModeIsActive(boolean isActionModeActive) {
		mIsActionModeActive = isActionModeActive;
	}

	@Override
	public boolean isActionModeActive() {
		return mIsActionModeActive;
	}

	@Override
	public void openNewAccount(long accountID) {
		((AccountListFragment) getFragmentManager().findFragmentById(
				R.id.account_list)).restartLocalLoader();
		onItemSelected(accountID);
	}

	@Override
	public void onSearchQuerySubmitted(String query, int searchMode) {
		Bundle args = new Bundle();
		args.putLong(AccountDetailFragment.ARG_ACCOUNT_ID, mID);
		args.putString(SearchResultsListFragment.ARG_SEARCH_QUERY, query);
		args.putInt(SearchResultsListFragment.ARG_SEARCH_MODE, searchMode);
		SearchResultsListFragment fragment = new SearchResultsListFragment();
		fragment.setArguments(args);
		backStackID = getFragmentManager()
				.beginTransaction()
				.replace(R.id.account_detail_container, fragment,
						"Account " + mID).addToBackStack(null).commit();
	}

	@Override
	public void backToAccount() {
		getFragmentManager().popBackStack(backStackID, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		backStackID = -1;
	}

	@Override
	public boolean isSearchResultsAttached() {
		return mIsSearchDisplayed;
	}

	@Override
	public void setIfSearchResultsAttached(boolean isAttached) {
		mIsSearchDisplayed = isAttached;
	}

	@Override
	public void restartLoader() {
		((AccountListFragment) getFragmentManager().findFragmentById(R.id.account_list)).restartLocalLoader();
	}
}
