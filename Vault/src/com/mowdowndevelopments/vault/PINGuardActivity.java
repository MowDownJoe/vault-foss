package com.mowdowndevelopments.vault;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.preference.*;

public class PINGuardActivity extends Activity implements
		OnEditorActionListener {

	SharedPreferences prefs;
	EditText pinBox;
	int pinAttempts = 0;
	long accountID;
	private final int maxPINAttempts = 5;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setSubtitle(R.string.prompt_pin);
		AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		PendingIntent intent = PendingIntent.getBroadcast(this, R.id.RepeatingBroadcastSetter, new Intent(this, UpdateDatabaseReceiver.class), 0);
		GregorianCalendar cal = new GregorianCalendar();
		cal.roll(Calendar.DAY_OF_YEAR, true);
		cal.set(Calendar.HOUR_OF_DAY, 2);
		manager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), TransactionRepetitionSimpleDialogFragment.millisecondsInDay, intent);
		PreferenceManager.setDefaultValues(this, R.xml.preferences_security,
				false);
		PreferenceManager.setDefaultValues(this, R.xml.preferences_display,
				false);
		PreferenceManager.setDefaultValues(this,
				R.xml.preferences_repeat_transactions, false);
		accountID = getIntent().getLongExtra(
				AccountDetailFragment.ARG_ACCOUNT_ID, -42);
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		if (prefs.getBoolean("enablePIN", false)) {
			setContentView(R.layout.activity_pinguard);
			pinBox = (EditText) findViewById(R.id.pinEntry);
			pinBox.setOnEditorActionListener(this);
		} else {
			throwToNext();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_pinguard, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.unlockPinScreen:
			tryToUnlock();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	protected void tryToUnlock() {
		String pin = prefs.getString("PINEntry", "0000");
		if (pin.equals(pinBox.getText().toString())) {
			throwToNext();
		} else {
			Toast.makeText(this, R.string.BadPIN, Toast.LENGTH_SHORT).show();
			++pinAttempts;
			if (pinAttempts == (maxPINAttempts - 1)) {
				findViewById(R.id.lastChanceTextView).setVisibility(
						View.VISIBLE);
			} else if (pinAttempts == maxPINAttempts) {
				finish();
			}
		}
	}

	protected void throwToNext() {
		if (accountID == -42) {
			accountID = VaultDatabaseHelper.getInstance(this)
					.getDefaultAccount();
		}
		Intent intent = new Intent();
		if (accountID == -1) {
			intent.setClass(this, AccountListActivity.class);
			startActivity(intent);
		} else {
			intent.putExtra(AccountDetailFragment.ARG_ACCOUNT_ID, accountID);
			// Large form-factor devices (tablets) typically have a width of 600
			// or greater density-independent points (dp).
			if (getResources().getConfiguration().smallestScreenWidthDp >= 600) {
				intent.setClass(this, AccountListActivity.class).putExtra(
						TransactionCreationActivity.ARG_WITH_EXTRA_ID, true);
				startActivity(intent);
			} else {
				intent.setClass(this, AccountDetailActivity.class);
				TaskStackBuilder
						.create(this)
						.addNextIntent(
								new Intent(this, AccountListActivity.class))
						.addNextIntent(intent).startActivities();
			}
		}
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		Log.d(this.getLocalClassName(), "actionID: "+actionId+"\nKeyEvent: "+event);
		if (event != null) {
			if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER
					&& event.getAction() == KeyEvent.ACTION_DOWN) {
				tryToUnlock();
				return true;
			}
		}
		return false;
	}

}
