package com.mowdowndevelopments.vault;

import org.apache.commons.lang3.ArrayUtils;

import com.commonsware.cwac.loaderex.SQLiteCursorLoader;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

public class RepeatingTransactionsListFragment extends ListFragment implements
		LoaderCallbacks<Cursor>, MultiChoiceModeListener, OnItemLongClickListener {

	SQLiteCursorLoader loader;
	RepeatTransactionCursorAdapter adapter;
	ActionMode mActionMode;

	public RepeatingTransactionsListFragment() {
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		adapter = new RepeatTransactionCursorAdapter(getActivity(), null);
		setListAdapter(adapter);
		setListShown(false);
		setEmptyText(getActivity().getString(R.string.noRepeats));
		LoaderManager.enableDebugLogging(true);
		getLoaderManager().initLoader(R.id.repeatTransactionListLoader, null, this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		ListView list = getListView();
		list.setMultiChoiceModeListener(this);
		list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		Long[] ids = ArrayUtils.toObject(getListView().getCheckedItemIds());
		switch (item.getItemId()){
		case R.id.confirmStop:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.unsetTransactionRepetition(ids[0]);
			} else {
				new BulkRepeatingTransactionStopper(getActivity()).execute(ids);
			}
			mode.finish();
			return true;
		}
		return false;
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		MenuInflater inflater = mode.getMenuInflater();
		inflater.inflate(R.menu.action_mode_repeating_transactions, menu);
		return true;
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
		mActionMode = null;
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	}

	@Override
	public void onItemCheckedStateChanged(ActionMode mode, int position,
			long id, boolean checked) {
		int checkedCount = getListView().getCheckedItemCount();
		if (checkedCount==1){
			mode.setSubtitle(R.string.oneFooSelected);
		} else {
			mode.setSubtitle(new StringBuilder().append(checkedCount).append(getActivity().getString(R.string.foo_quantity_selected)).toString());
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		loader = new SQLiteCursorLoader(getActivity(),
				VaultDatabaseHelper.getInstance(getActivity()),
				RepeatTransactionCursorAdapter.LOADEREX_INIT_QUERY, null);
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> l, Cursor c) {
		adapter.changeCursor(c);
		if (isResumed()){
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> l) {
		adapter.changeCursor(null);
		setListShown(false);
	}
	
	protected void restartLocalLoader(){
		getLoaderManager().restartLoader(R.id.repeatTransactionListLoader, null, this);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> listView, View v, int position,
			long id) {
		if (mActionMode == null){
			mActionMode = getActivity().startActionMode(this);
			return true;
		}
		return false;
	}

	public class BulkRepeatingTransactionStopper extends AsyncTask<Long, Integer, Void> {
	
		ProgressDialog dialog;
		Context cxt;
		/**
		 * @param cxt
		 */
		public BulkRepeatingTransactionStopper(Context cxt) {
			super();
			this.cxt = cxt;
		}
		@Override
		protected Void doInBackground(Long... params) {
			int length = params.length;
			for (int i = 0; i < length; i++) {
				VaultDatabaseHelper.getInstance(cxt)
						.unsetTransactionRepetition(params[i]);
				publishProgress((int) ((i / (float) length) * 100));
			}
			dialog.dismiss();
			return null;
		}
		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onProgressUpdate(Progress[])
		 */
		@Override
		protected void onProgressUpdate(Integer... values) {
			if (dialog == null){
				dialog = ProgressDialog.show(cxt, "", cxt.getString(R.string.erasing), false, false);
			}
			dialog.setProgress(values[0]);
		}
		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			restartLocalLoader();
		}
	
	}

}
