package com.mowdowndevelopments.vault;

import java.text.NumberFormat;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

public class AccountListCursorAdapter extends ResourceCursorAdapter {

	final static String ALT_LOADEREX_INIT_QUERY = "select "
			+ VaultDatabaseHelper.ID + " as " + VaultDatabaseHelper.REALID
			+ ", " + VaultDatabaseHelper.DEFAULT + ", "
			+ VaultDatabaseHelper.ACCTNAME + " from "
			+ VaultDatabaseHelper.ACCTTABLE;

	int layout;

	public AccountListCursorAdapter(Context context, int layout, Cursor c,
			boolean autoRequery) {
		super(context, layout, c, autoRequery);
		this.layout = layout;
	}

	public AccountListCursorAdapter(Context context, int layout, Cursor c,
			int flags) {
		super(context, layout, c, flags);
		this.layout = layout;
	}

	public AccountListCursorAdapter(Context context, Cursor c) {
		super(context, R.layout.account_list_item, c, false);
		this.layout = R.layout.account_list_item;
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(layout, parent, false);
		TextView nameView = (TextView) v.findViewById(R.id.accountNameView);
		TextView balanceView = (TextView) v.findViewById(R.id.balanceView);
		// When you're targeting 4.0 and later, View.setTag() does all the work
		// of ViewHolder with less fuss.
		v.setTag(R.id.favStar, v.findViewById(R.id.favStar));
		v.setTag(R.id.accountNameView, nameView);
		v.setTag(R.id.balanceView, balanceView);
		return v;
	}

	@Override
	public void bindView(View v, Context context, Cursor c) {
		ImageView star = (ImageView) v.getTag(R.id.favStar);
		TextView balanceView = (TextView) v.getTag(R.id.balanceView);
		TextView nameView = (TextView) v.getTag(R.id.accountNameView);
		long currentID = c
				.getLong(c.getColumnIndex(VaultDatabaseHelper.REALID));
		if (c.getShort(c.getColumnIndexOrThrow(VaultDatabaseHelper.DEFAULT)) == 1) {
			star.setVisibility(View.VISIBLE);
		} else {
			star.setVisibility(View.GONE);
		}
		nameView.setText(c.getString(c
				.getColumnIndexOrThrow(VaultDatabaseHelper.ACCTNAME)));
		balanceView.setText(NumberFormat.getCurrencyInstance().format(
				VaultDatabaseHelper.getInstance(context).getCurrentBalance(
						currentID)));
	}

}
