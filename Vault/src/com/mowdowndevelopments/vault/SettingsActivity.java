package com.mowdowndevelopments.vault;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.preference.PreferenceActivity;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;

import java.util.List;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {
	/**
	 * Determines whether to always show the simplified settings UI, where
	 * settings are presented in a single list. When false, settings are shown
	 * as a master/detail two-pane view on tablets. When true, a single pane is
	 * shown on tablets.
	 */


	static final String ARG_SOURCE_ACTIVITY_NAME = "com.mowdowndevelopments.vault.sourceActivity";
	
	@Override
	public void onBuildHeaders(List<Header> target) {
		loadHeadersFromResource(R.xml.preference_header, target);
		setupActionBar();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// Show the Up button in the action bar.
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			// TODO: If Settings has multiple levels, Up should navigate up
			// that hierarchy.
			Intent outIntent, sourceIntent = getIntent();
			boolean cameWithExtraID = sourceIntent.getBooleanExtra(TransactionCreationActivity.ARG_WITH_EXTRA_ID, false);
			try {
				outIntent = new Intent(this, Class.forName(sourceIntent.getStringExtra(ARG_SOURCE_ACTIVITY_NAME)));
			} catch (ClassNotFoundException e) {
				getActionBar().setHomeButtonEnabled(false);
				e.printStackTrace();
				return true;
			}
			if (cameWithExtraID){
				outIntent.putExtra(AccountDetailFragment.ARG_ACCOUNT_ID, sourceIntent.getLongExtra(AccountDetailFragment.ARG_ACCOUNT_ID, -1));
			}
			outIntent.putExtra(TransactionCreationActivity.ARG_WITH_EXTRA_ID, cameWithExtraID);
			if (outIntent != null) {
				NavUtils.navigateUpTo(this, outIntent);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
