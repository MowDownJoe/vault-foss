package com.mowdowndevelopments.vault;

import java.text.NumberFormat;
import java.text.ParseException;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class ModifyAccountDialogFragment extends DialogFragment implements
		OnClickListener {

	public interface OnAccountEdited {
		void restartLoader();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		try {
			callbacks = (OnAccountEdited) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnAccountEdited");
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		callbacks = null;
	}

	public ModifyAccountDialogFragment() {
		// Required empty public constructor
	}

	final static String ARG_EDITTING = "com.mowdowndevelopments.vault.to_edit",
			ARG_NAME = "com.mowdowndevelopments.vault.name",
			ARG_BAL = "com.mowdowndevelopments.vault.balance";
	String argument;
	long accountID;
	EditText textBox;
	OnAccountEdited callbacks = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.dialog_fragment_modify_account,
				container, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Fragment#onViewCreated(android.view.View,
	 * android.os.Bundle)
	 */
	@Override
	public void onViewCreated(View v, Bundle savedInstanceState) {
		super.onViewCreated(v, savedInstanceState);
		v.findViewById(R.id.modifyButton).setOnClickListener(this);
		v.findViewById(R.id.cancelButton).setOnClickListener(this);
		TextView title = (TextView) v.findViewById(R.id.dialogLabel);
		textBox = (EditText) v.findViewById(R.id.dialogTextBox);
		Bundle args = getArguments();
		if (args != null) {
			argument = args.getString(ARG_EDITTING);
			accountID = args.getLong(AccountDetailFragment.ARG_ACCOUNT_ID);
			if (argument.equalsIgnoreCase(ARG_BAL)) {
				title.setText(R.string.startBalance);
				textBox.setRawInputType(InputType.TYPE_CLASS_NUMBER
						| InputType.TYPE_NUMBER_FLAG_DECIMAL);
				textBox.setText(Double.toString(VaultDatabaseHelper
						.getInstance(getActivity())
						.getOpeningBalance(accountID)));
				getDialog().setTitle(R.string.editValue);
			} else if (argument.equalsIgnoreCase(ARG_NAME)) {
				title.setText(R.string.accountName);
				textBox.setRawInputType(InputType.TYPE_CLASS_TEXT);
				textBox.setText(VaultDatabaseHelper.getInstance(getActivity())
						.getAccountName(accountID));
				getDialog().setTitle(R.string.editName);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.modifyButton:
			String value = textBox.getText().toString().trim();
			if (value.contains(";") || value.contains("--")
					|| value.contains(")") || value.contains("\"")) {
				Toast.makeText(getActivity(), R.string.noSpecialCharacters,
						Toast.LENGTH_LONG).show();
				break;
			}
			if (argument.equalsIgnoreCase(ARG_BAL)) {
				NumberFormat formatter = NumberFormat.getInstance();
				formatter.setMaximumFractionDigits(2);
				try {
					VaultDatabaseHelper.getInstance(getActivity())
							.updateOpeningBalance(formatter.parse(value).doubleValue(),
									accountID);
					dismiss();
					callbacks.restartLoader();
				} catch (ParseException e) {
					Toast.makeText(getActivity(), R.string.invalidBalArg,
							Toast.LENGTH_SHORT).show();
				}

			} else if (argument.equalsIgnoreCase(ARG_NAME)) {
				if (!value.isEmpty() || !value.equals(null)) {
					VaultDatabaseHelper.getInstance(getActivity())
							.renameAccount(value, accountID);
					dismiss();
					callbacks.restartLoader();
				} else {
					Toast.makeText(getActivity(), R.string.invalidBalArg,
							Toast.LENGTH_SHORT).show();
				}
			}
			break;
		case R.id.cancelButton:
			dismiss();
			break;
		}
	}

}
