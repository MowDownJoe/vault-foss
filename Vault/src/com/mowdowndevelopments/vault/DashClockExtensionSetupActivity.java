package com.mowdowndevelopments.vault;

import com.commonsware.cwac.loaderex.SQLiteCursorLoader;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.database.Cursor;

public class DashClockExtensionSetupActivity extends ListActivity implements
		LoaderCallbacks<Cursor> {

	static final String ARG_PREFS_DASHCLOCK = "com.mowdowndevelopments.vault.dashclock_prefs",
			ARG_DASHCLOCK_TO_DISPLAY = "com.mowdowndevelopments.vault.account_to_display";
	SQLiteCursorLoader loader;
	AccountListCursorAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter = new AccountListCursorAdapter(this, null);
		setListAdapter(adapter);
		getListView().setVisibility(View.INVISIBLE);
		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		LoaderManager.enableDebugLogging(true);
		getLoaderManager().initLoader(R.id.DashClockSetupLoader, null, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		loader = new SQLiteCursorLoader(this,
				VaultDatabaseHelper.getInstance(this),
				AccountListCursorAdapter.ALT_LOADEREX_INIT_QUERY, null);
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> l, Cursor cursor) {
		adapter.changeCursor(cursor);
		getListView().setVisibility(View.VISIBLE);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onLoaderReset(Loader<Cursor> l) {
		adapter.changeCursor(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.ListActivity#onListItemClick(android.widget.ListView,
	 * android.view.View, int, long)
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		getSharedPreferences(ARG_PREFS_DASHCLOCK, 0).edit()
				.putLong(ARG_DASHCLOCK_TO_DISPLAY, id).commit();
		finish();
	}

}
