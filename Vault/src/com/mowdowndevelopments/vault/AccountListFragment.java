package com.mowdowndevelopments.vault;

import org.apache.commons.lang3.ArrayUtils;

import android.app.ActionBar;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.os.Bundle;
import android.app.ListFragment;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.app.LoaderManager;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.commonsware.cwac.loaderex.SQLiteCursorLoader;

/**
 * A list fragment representing a list of Accounts. This fragment also supports
 * tablet devices by allowing list items to be given an 'activated' state upon
 * selection. This helps indicate which item is currently being viewed in a
 * {@link AccountDetailFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class AccountListFragment extends ListFragment implements
		LoaderCallbacks<Cursor>, MultiChoiceModeListener,
		OnItemLongClickListener {

	private SQLiteCursorLoader loader;
	private AccountListCursorAdapter adapter;
	protected ActionBar mActionBar;
	private ActionMode mActionMode;
	private AQuery aq;
	/**
	 * The current activated item position. Only used on tablets.
	 */
	private int mActivatedPosition = ListView.INVALID_POSITION;
	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * activated item position. Only used on tablets.
	 */
	private static final String STATE_ACTIVATED_POSITION = "activated_position";
	private static final String STATE_FIRST_VISIBLE_POSITION = "visible_position";

	/**
	 * The fragment's current callback object, which is notified of list item
	 * clicks.
	 */
	private Callbacks mCallbacks = sDummyCallbacks;

	/**
	 * A callback interface that all activities containing this fragment must
	 * implement. This mechanism allows activities to be notified of item
	 * selections.
	 */
	public interface Callbacks extends MainScreensCallbacksBase {
		/**
		 * Callback for when an item has been selected. Change argument to
		 * accept longs, then resolve errors.
		 */
		public void onItemSelected(long id);
	}

	/**
	 * A dummy implementation of the {@link Callbacks} interface that does
	 * nothing. Used only when this fragment is not attached to an activity.
	 */
	private static Callbacks sDummyCallbacks = new Callbacks() {
		@Override
		public void onItemSelected(long id) {
		}

		@Override
		public void setIfActionModeIsActive(boolean isActionModeActive) {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean isActionModeActive() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isDualPane() {
			// TODO Auto-generated method stub
			return false;
		}
	};

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public AccountListFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter = new AccountListCursorAdapter(getActivity(), null);
		setHasOptionsMenu(true);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_account_list, container,
				false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		aq = new AQuery(getActivity(), view);
		getListView().setMultiChoiceModeListener(this);
		// Restore the previously serialized activated item position.
		if (savedInstanceState != null
				&& savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState
					.getInt(STATE_ACTIVATED_POSITION));
		}
		setListAdapter(adapter);
		getListView().setVisibility(View.INVISIBLE);
		aq.id(R.id.loadingReadyRun).visible();
		LoaderManager.enableDebugLogging(true);
		getLoaderManager().initLoader(R.id.AccountListLoader, null, this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceBundle) {
		super.onActivityCreated(savedInstanceBundle);
		if (mCallbacks.isDualPane()) {
			aq.id(R.id.addAccountButton).visible()
					.clicked(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							newAccount();
						}
					});
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.fragment_account_list, menu);
		if (mCallbacks.isDualPane()) {
			menu.findItem(R.id.newAcct).setVisible(false);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case (R.id.newAcct):
			newAccount();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException(
					"Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();

		// Reset the active callbacks interface to the dummy implementation.
		mCallbacks = sDummyCallbacks;
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position,
			long id) {
		super.onListItemClick(listView, view, position, id);
		if (!mCallbacks.isActionModeActive() && mActionMode == null) {
			// Notify the active callbacks interface (the activity, if the
			// fragment is attached to one) that an item has been selected.
			mCallbacks.onItemSelected(id);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			// Serialize and persist the activated item position.
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
		outState.putInt(STATE_FIRST_VISIBLE_POSITION, getListView()
				.getFirstVisiblePosition());
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		loader = new SQLiteCursorLoader(this.getActivity(),
				VaultDatabaseHelper.getInstance(getActivity()),
				AccountListCursorAdapter.ALT_LOADEREX_INIT_QUERY, null);
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor c) {
		adapter.changeCursor(c);
		aq.id(R.id.loadingReadyRun).gone();
		if (getListView().getCount() == 0) {
			if (isResumed()) {
				aq.id(R.id.noAccounts).visible()
						.animate(android.R.anim.fade_in);
			} else {
				aq.id(R.id.noAccounts).visible();
			}
		} else if (isResumed()) {
			aq.id(android.R.id.list).visible()
					.animate(android.R.anim.slide_in_left);
		} else {
			getListView().setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		adapter.changeCursor(null);
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		Long[] ids = ArrayUtils.toObject(getListView().getCheckedItemIds());
		switch (item.getItemId()) {
		case R.id.rename:
			ModifyAccountDialogFragment fragment = new ModifyAccountDialogFragment();
			Bundle bundle = new Bundle();
			bundle.putString(ModifyAccountDialogFragment.ARG_EDITTING,
					ModifyAccountDialogFragment.ARG_NAME);
			bundle.putLong(AccountDetailFragment.ARG_ACCOUNT_ID, ids[0]);
			fragment.setArguments(bundle);
			fragment.show(getFragmentManager(), "modify");
			return true;
		case R.id.changeInitBal:
			ModifyAccountDialogFragment dialog = new ModifyAccountDialogFragment();
			Bundle args = new Bundle();
			args.putString(ModifyAccountDialogFragment.ARG_EDITTING,
					ModifyAccountDialogFragment.ARG_BAL);
			args.putLong(AccountDetailFragment.ARG_ACCOUNT_ID, ids[0]);
			dialog.setArguments(args);
			dialog.show(getFragmentManager(), "modify");
			return true;
		case R.id.toggleDefault:
			VaultDatabaseHelper.getInstance(getActivity()).toggleIfDefault(
					ids[0].longValue());
			restartLocalLoader();
			mode.finish();
			return true;
		case R.id.confirmDeleteAccount:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity()).deleteAccount(
						ids[0].longValue());
				restartLocalLoader();
			} else {
				new BulkAccountEraser(getActivity()).execute(ids);
			}
			mode.finish();
			return true;
		default:
			return false;
		}
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		MenuInflater inflater = mode.getMenuInflater();
		inflater.inflate(R.menu.action_mode_account_list, menu);
		mCallbacks.setIfActionModeIsActive(true);
		return true;
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
		mActionMode = null;
		mCallbacks.setIfActionModeIsActive(false);
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		mode.setTitle(getActivity().getActionBar().getTitle());
		return true;
	}

	@Override
	public void onItemCheckedStateChanged(ActionMode mode, int position,
			long id, boolean checked) {
		if (getListView().getCheckedItemCount() == 1) {
			mode.getMenu().findItem(R.id.toggleDefault).setVisible(true);
			mode.getMenu().findItem(R.id.editAcct).setVisible(true);
			mode.setSubtitle(R.string.CABAcctSubSingular);
		} else {
			mode.getMenu().findItem(R.id.toggleDefault).setVisible(false);
			mode.getMenu().findItem(R.id.editAcct).setVisible(false);
			mode.setSubtitle(new StringBuilder()
					.append(getListView().getCheckedItemCount()).append(" ")
					.append(getActivity().getString(R.string.CABAcctSub))
					.toString());
		}

	}

	@Override
	public boolean onItemLongClick(AdapterView<?> listView, View v,
			int position, long id) {

		if (mActionMode != null || mCallbacks.isActionModeActive()) {
			return false;
		}
		mActionMode = getActivity().startActionMode(this);
		return true;
	}

	protected void newAccount() {
		new NewAccountDialogFragment().show(getFragmentManager(),
				"AccountCreation");
	}

	public void restartLocalLoader() {
		if (getListView().getCount() == 0) {
			aq.id(R.id.noAccounts).gone();
			aq.id(R.id.loadingReadyRun).visible();
		}
		getLoaderManager().restartLoader(2228, null, this);
	}

	public class BulkAccountEraser extends BulkEraser {

		public BulkAccountEraser(Context c) {
			super(c, true);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			restartLocalLoader();
		}

	}

}
