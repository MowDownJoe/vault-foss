package com.mowdowndevelopments.vault;

import java.util.Calendar;
import java.util.LinkedList;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.preference.PreferenceManager;

public class DatabaseUpdatingService extends IntentService {
	public DatabaseUpdatingService() {
		super("DatabaseUpdatingSerivce");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		synchronized (this) {
			Long[] repeatTransIDs = VaultDatabaseHelper.getInstance(this)
					.getRepeatingTransactions();
			if (repeatTransIDs != null) {
				LinkedList<Long> idList = new LinkedList<Long>();
				Calendar cal = Calendar.getInstance();
				long repeatDelay, transactionDate, newTransID;
				SharedPreferences prefs = PreferenceManager
						.getDefaultSharedPreferences(this);
				final long displayDelay = prefs.getInt("timeToReveal", 0)
						* TransactionRepetitionSimpleDialogFragment.millisecondsInDay;
				Cursor c;
				boolean toNotify = false;
				int length = repeatTransIDs.length;
				for (int i = 0; i < length; i++) {
					c = VaultDatabaseHelper.getInstance(this)
							.getRepeatingTransactionData(repeatTransIDs[i]);
					repeatDelay = c
							.getLong(c
									.getColumnIndexOrThrow(VaultDatabaseHelper.DELAYMILLIS));
					transactionDate = VaultDatabaseHelper.getInstance(this)
							.getTransactionDate(repeatTransIDs[i]);
					if ((repeatDelay + transactionDate - displayDelay) > cal
							.getTimeInMillis()) {
						newTransID = VaultDatabaseHelper.getInstance(this)
								.replicateTransaction(repeatTransIDs[i],
										displayDelay);
						if (VaultDatabaseHelper.getInstance(this)
								.doesRepeatingTransactionNotify(
										repeatTransIDs[i])) {
							toNotify = true;
							idList.add(repeatTransIDs[i]);
						}
						VaultDatabaseHelper
								.getInstance(this)
								.reassignRepeat(
										c.getLong(c
												.getColumnIndexOrThrow(VaultDatabaseHelper.ID)),
										newTransID);
					}
					c.close();
				}
				if (toNotify) {
					Long[] idsToNotify = idList
							.toArray(new Long[idList.size()]);
					int noOfIDs = idsToNotify.length;
					NotificationManager manager = (NotificationManager) this
							.getSystemService(Context.NOTIFICATION_SERVICE);
					Notification.Builder builder = new Notification.Builder(
							this)
							.setSmallIcon(R.drawable.ic_stat_notify)
							.setTicker(this.getString(R.string.notify_title))
							.setContentTitle(
									this.getString(R.string.notify_title))
							.setContentIntent(
									PendingIntent.getActivity(this, R.id.pendingIntentRequestCode,
											new Intent(this,
													PINGuardActivity.class), 0))
							.setContentInfo(
									VaultDatabaseHelper
											.getInstance(this)
											.getTransactionParty(idsToNotify[0])
											+ "+" + (noOfIDs - 1))
							.setAutoCancel(true);
					if (prefs.getBoolean("allowLED", false)) {
						builder.setLights(
								prefs.getInt("LEDColor", R.color.color_green),
								750, 750);
					}
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
						manager.notify(R.id.notification,
								getExpandableNotification(idsToNotify, builder));
					} else {
						manager.notify(R.id.notification, builder.getNotification());
					}
				}
			}
		}
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private Notification getExpandableNotification(Long[] ids,
			Notification.Builder builder) {
		int length = ids.length;
		builder.setPriority(Notification.PRIORITY_LOW);
		Notification.InboxStyle inboxBuilder = new Notification.InboxStyle(
				builder);
		for (int i = 0; i < length || i < 4; i++) {
			inboxBuilder.addLine(VaultDatabaseHelper.getInstance(this)
					.getTransactionParty(ids[i]));
		}
		inboxBuilder.setSummaryText(VaultDatabaseHelper.getInstance(this)
				.getTransactionParty(ids[0]) + "+" + (length - 1));
		return inboxBuilder.build();
	}
}
