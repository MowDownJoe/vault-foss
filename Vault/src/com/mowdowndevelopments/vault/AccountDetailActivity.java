package com.mowdowndevelopments.vault;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/**
 * An activity representing a single Account detail screen. This activity is
 * only used on handset devices. On tablet-size devices, item details are
 * presented side-by-side with a list of items in a {@link AccountListActivity}.
 * <p>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link AccountDetailFragment}.
 */
public class AccountDetailActivity extends Activity implements
		AccountDetailFragment.Callbacks, SearchResultsListFragment.Callbacks {

	private long mID;
	private boolean mIsActionModeActive, mIsSearchDisplayed = false;
	int backStackID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account_detail);

		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(true);

		// savedInstanceState is non-null when there is fragment state
		// saved from previous configurations of this activity
		// (e.g. when rotating the screen from portrait to landscape).
		// In this case, the fragment will automatically be re-added
		// to its container so we don't need to manually add it.
		// For more information, see the Fragments API guide at:
		//
		// http://developer.android.com/guide/components/fragments.html
		//
		if (savedInstanceState == null) {
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
			Bundle arguments = new Bundle();
			mID = getIntent().getLongExtra(
					AccountDetailFragment.ARG_ACCOUNT_ID, -1);
			arguments.putLong(AccountDetailFragment.ARG_ACCOUNT_ID, mID);
			AccountDetailFragment fragment = new AccountDetailFragment();
			fragment.setArguments(arguments);
			getFragmentManager()
					.beginTransaction()
					.add(R.id.account_detail_container, fragment,
							"Account " + mID).commit();

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.generic_activity_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.settingsMenuItem:
			intent = new Intent(this, SettingsActivity.class)
					.putExtra(AccountDetailFragment.ARG_ACCOUNT_ID, mID)
					.putExtra(TransactionCreationActivity.ARG_WITH_EXTRA_ID, true)
					.putExtra(SettingsActivity.ARG_SOURCE_ACTIVITY_NAME,
							this.getClass().getName());
			startActivity(intent);
			return true;
		case R.id.manageRepeats:
			intent = new Intent(this, RepeatingTransactionsActivity.class)
					.putExtra(AccountDetailFragment.ARG_ACCOUNT_ID, mID)
					.putExtra(TransactionCreationActivity.ARG_WITH_EXTRA_ID, true)
					.putExtra(SettingsActivity.ARG_SOURCE_ACTIVITY_NAME,
							this.getClass().getName());
			startActivity(intent);
			return true;
		case R.id.reportIssue:
			intent = new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://bitbucket.org/MowDownJoe/vault/issues"))
					.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			return true;
		case R.id.about:
			new AboutDialogFragment().show(getFragmentManager(), "AboutThis");
			return true;
		case android.R.id.home:
			/*
			 * This ID represents the Home or Up button. In the case of this
			 * activity, the Up button is shown. Use NavUtils to allow users to
			 * navigate up one level in the application structure. For more
			 * details, see the Navigation pattern on Android Design:
			 * 
			 * http://developer.android.com/design/patterns/navigation.html#up-vs
			 * -back
			 */
			NavUtils.navigateUpTo(this, new Intent(this,
					AccountListActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onCheckboxClicked(View v) {
		if (mIsSearchDisplayed) {
			((SearchResultsListFragment) getFragmentManager()
					.findFragmentByTag("Account " + mID)).onCheckboxClicked(v);
		} else {
			((AccountDetailFragment) getFragmentManager().findFragmentByTag(
					"Account " + mID)).onCheckboxClicked(v);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// VaultDatabaseHelper.getInstance(getApplication()).close();
	}

	@Override
	public void setIfActionModeIsActive(boolean isActionModeActive) {
		// TODO Auto-generated method stub
		mIsActionModeActive = isActionModeActive;
	}

	@Override
	public boolean isActionModeActive() {
		// TODO Auto-generated method stub
		return mIsActionModeActive;
	}

	@Override
	public boolean isDualPane() {
		return false;
	}

	@Override
	public void onSearchQuerySubmitted(String query, int searchMode) {
		Bundle args = new Bundle();
		args.putLong(AccountDetailFragment.ARG_ACCOUNT_ID, mID);
		args.putString(SearchResultsListFragment.ARG_SEARCH_QUERY, query);
		args.putInt(SearchResultsListFragment.ARG_SEARCH_MODE, searchMode);
		SearchResultsListFragment fragment = new SearchResultsListFragment();
		fragment.setArguments(args);
		backStackID = getFragmentManager()
				.beginTransaction()
				.replace(R.id.account_detail_container, fragment,
						"Account " + mID).addToBackStack(null).commit();
	}

	@Override
	public void backToAccount() {
		getFragmentManager().popBackStack(backStackID, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		backStackID = -1;
	}

	@Override
	public boolean isSearchResultsAttached() {
		return mIsSearchDisplayed;
	}

	@Override
	public void setIfSearchResultsAttached(boolean isAttached) {
		mIsSearchDisplayed = isAttached;
	}
}
