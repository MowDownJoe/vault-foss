package com.mowdowndevelopments.vault;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.ArrayUtils;

import com.commonsware.cwac.loaderex.SQLiteCursorLoader;
import android.app.Activity;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

public class SearchResultsListFragment extends ListFragment implements
		MultiChoiceModeListener, LoaderCallbacks<Cursor>,
		OnItemLongClickListener {

	public interface Callbacks extends MainScreensCallbacksBase {
		public void backToAccount();

		public boolean isSearchResultsAttached();

		public void setIfSearchResultsAttached(boolean isAttached);
	}

	public class BulkTransactionEraser extends BulkEraser {

		public BulkTransactionEraser(Context c) {
			super(c, false);
			// TODO Auto-generated constructor stub
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			restartLocalLoader();
		}

	}

	public class BulkReplicator extends AsyncTask<Long, Integer, Void> {

		Context cxt;
		long delay;
		ProgressDialog dialog;

		public BulkReplicator(Context c, long delay) {
			cxt = c;
			this.delay = delay;
		}

		@Override
		protected Void doInBackground(Long... params) {
			int length = params.length;
			for (int i = 0; i < length; i++) {
				VaultDatabaseHelper.getInstance(cxt).replicateTransaction(
						params[i], delay);
				publishProgress((int) ((i / (float) length) * 100));
				if (isCancelled()) {
					break;
				}
			}
			dialog.dismiss();
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onProgressUpdate(Progress[])
		 */
		@Override
		protected void onProgressUpdate(Integer... values) {
			if (dialog == null) {
				dialog = ProgressDialog.show(cxt, "",
						cxt.getString(R.string.replicate), false, false);
			}
			dialog.setProgress(values[0]);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			restartLocalLoader();
		}

	}

	final static String ARG_SEARCH_QUERY = "com.mowdowndevelopments.vault.search_query";
	final static String ARG_SEARCH_MODE = "com.mowdowndevelopments.vault.search_mode";
	static final String SEARCH_FOR_NAME_LOADER_QUERY = TransactionListCursorAdapter.LOADEREX_INIT_QUERY
			+ " and " + VaultDatabaseHelper.PARTYNAME + " LIKE ?";
	final static String SEARCH_FOR_TAG_INIT_QUERY = "select t."
			+ VaultDatabaseHelper.ID + " as " + VaultDatabaseHelper.REALID
			+ ", t." + VaultDatabaseHelper.ISCHECKED + " as "
			+ VaultDatabaseHelper.ISCHECKED + ", t."
			+ VaultDatabaseHelper.DATEINMILLIS + " as "
			+ VaultDatabaseHelper.DATEINMILLIS + ", p."
			+ VaultDatabaseHelper.PARTYNAME + " as "
			+ VaultDatabaseHelper.PARTYNAME + ", t."
			+ VaultDatabaseHelper.TRANSVALUE + " as "
			+ VaultDatabaseHelper.TRANSVALUE + ", t."
			+ VaultDatabaseHelper.TRANSTYPE + " as "
			+ VaultDatabaseHelper.TRANSTYPE + ", t."
			+ VaultDatabaseHelper.CHECKNO + " as "
			+ VaultDatabaseHelper.CHECKNO + " from "
			+ VaultDatabaseHelper.TRANSTABLE + " as t, "
			+ VaultDatabaseHelper.TAGUSETABLE + " as u, "
			+ VaultDatabaseHelper.PARTYTABLE + " as p" + " where p."
			+ VaultDatabaseHelper.ID + "=t." + VaultDatabaseHelper.PARTYLNK
			+ " and u." + VaultDatabaseHelper.TAGID + "=? " + "and u."
			+ VaultDatabaseHelper.TRANSID + " = " + VaultDatabaseHelper.REALID
			+ " and t." + VaultDatabaseHelper.LNKACCTID + "=?";
	// TODO Tweak tag search query to cross-reference tag usage.
	String query;
	long accountID;
	int searchMode;
	static final int searchArgName = 0, searchArgTag = 1;
	private Callbacks mCallbacks = null;

	SQLiteCursorLoader loader;
	TransactionListCursorAdapter adapter;
	private ActionMode mActionMode;

	public SearchResultsListFragment() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.ListFragment#onViewCreated(android.view.View,
	 * android.os.Bundle)
	 */
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		adapter = new TransactionListCursorAdapter(getActivity(), null);
		ListView list = getListView();
		list.setMultiChoiceModeListener(this);
		list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		setListAdapter(adapter);
		setListShown(false);
		setEmptyText(getActivity().getString(R.string.noSearchResults));
		LoaderManager.enableDebugLogging(true);
		switch (searchMode) {
		case searchArgTag:
			getLoaderManager().initLoader(R.id.TagSearchResultLoader, null,
					this);
			break;
		case searchArgName:
			getLoaderManager().initLoader(R.id.NameSearchResultLoader, null,
					this);
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Fragment#onActivityCreated(android.os.Bundle)
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		mCallbacks.setIfSearchResultsAttached(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Fragment#onAttach(android.app.Activity)
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallbacks = (Callbacks) activity;
		} catch (ClassCastException e) {
			throw new IllegalArgumentException(
					"Host activity must implement callbacks");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks.setIfSearchResultsAttached(false);
		mCallbacks = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Fragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		Bundle args = getArguments();
		if (args != null) {
			query = new StringBuilder().append('%').append(args.getString(ARG_SEARCH_QUERY)).append('%').toString();
			accountID = args.getLong(AccountDetailFragment.ARG_ACCOUNT_ID);
			searchMode = args.getInt(ARG_SEARCH_MODE);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Fragment#onCreateOptionsMenu(android.view.Menu,
	 * android.view.MenuInflater)
	 */
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.fragment_search_results, menu);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Fragment#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.backToAccount:
			mCallbacks.backToAccount();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		Long[] ids = ArrayUtils.toObject(getListView().getCheckedItemIds());
		switch (item.getItemId()) {
		case R.id.editTransaction:
			editTransaction(ids[0]);
			return true;
		case R.id.deleteTrans:
			Log.d("ActionModeIDs", Arrays.toString(ids));
			return false;
		case R.id.confirmDeleteTrans:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.deleteTransaction(ids[0]);
				restartLocalLoader();
			} else {
				new BulkTransactionEraser(getActivity()).execute(ids);
			}
			mode.finish();
			return true;
		case R.id.repeatTransMonth:
			GregorianCalendar gCal = new GregorianCalendar();
			gCal.roll(Calendar.MONTH, true);
			long delay = gCal.getTimeInMillis()
					- Calendar.getInstance().getTimeInMillis();
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.replicateTransaction(ids[0], delay);
				restartLocalLoader();
			} else {
				new BulkReplicator(getActivity(), delay).execute(ids);
			}
			mode.finish();
			return true;
		case R.id.repeatTransNow:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.replicateTransaction(ids[0], 0);
				restartLocalLoader();
			} else {
				new BulkReplicator(getActivity(), 0).execute(ids);
			}
			mode.finish();
			return true;
		case R.id.repeatTransOneWeek:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.replicateTransaction(ids[0], 604800000);
				restartLocalLoader();
			} else {
				new BulkReplicator(getActivity(), 604800000).execute(ids);
			}
			mode.finish();
			return true;
		case R.id.repeatTransTomorrow:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.replicateTransaction(ids[0], 86400000);
				restartLocalLoader();
			} else {
				new BulkReplicator(getActivity(), 86400000).execute(ids);
			}
			mode.finish();
			return true;
		case R.id.repeatTransTwoWeek:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.replicateTransaction(ids[0], 1209600000);
				restartLocalLoader();
			} else {
				new BulkReplicator(getActivity(), 1209600000).execute(ids);
			}
			mode.finish();
			return true;
		case R.id.repeatTransYear:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.replicateTransaction(ids[0], 31560000000l);
				restartLocalLoader();
			} else {
				new BulkReplicator(getActivity(), 31560000000l).execute(ids);
			}
			mode.finish();
			return true;
		default:
			return false;
		}
	}

	private void restartLocalLoader() {
		switch (searchMode) {
		case searchArgName:
			getLoaderManager().restartLoader(R.id.NameSearchResultLoader, null,
					this);
			break;
		case (searchArgTag):
			getLoaderManager().restartLoader(R.id.TagSearchResultLoader, null,
					this);
			break;
		}
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		MenuInflater inflater = mode.getMenuInflater();
		inflater.inflate(R.menu.action_mode_transaction_list, menu);
		mCallbacks.setIfActionModeIsActive(true);
		return true;
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
		mActionMode = null;
		mCallbacks.setIfActionModeIsActive(false);
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	}

	@Override
	public void onItemCheckedStateChanged(ActionMode mode, int position,
			long id, boolean checked) {
		if (getListView().getCheckedItemCount() == 1) {
			mode.getMenu().findItem(R.id.editTransaction).setVisible(true);
			mode.setSubtitle(R.string.CABTransSubSingular);
		} else {
			mode.getMenu().findItem(R.id.editTransaction).setVisible(false);
			mode.setSubtitle(new StringBuilder()
					.append(getListView().getCheckedItemCount()).append(" ")
					.append(getActivity().getString(R.string.CABTransSub))
					.toString());
		}

	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		if (id == R.id.TagSearchResultLoader) {
			long tagID = VaultDatabaseHelper.getInstance(getActivity())
					.getIDForTagLikeQuery(query);
			loader = new SQLiteCursorLoader(getActivity(),
					VaultDatabaseHelper.getInstance(getActivity()),
					SEARCH_FOR_TAG_INIT_QUERY, new String[] {
							Long.toString(tagID), Long.toString(accountID) });
		} else if (id == R.id.NameSearchResultLoader) {
			loader = new SQLiteCursorLoader(getActivity(),
					VaultDatabaseHelper.getInstance(getActivity()),
					SEARCH_FOR_NAME_LOADER_QUERY, new String[] {
							Long.toString(accountID), query });
		} else {
			return null;
		}
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor c) {
		adapter.changeCursor(c);
		if (isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		adapter.changeCursor(null);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> listView, View v,
			int position, long id) {
		if (mActionMode != null || mCallbacks.isActionModeActive()) {
			return false;
		}
		mActionMode = getActivity().startActionMode(this);
		return true;
	}

	public void editTransaction(long transID) {
		Intent intent = new Intent(this.getActivity(),
				TransactionCreationActivity.class)
				.putExtra(AccountListActivity.ARG_DUAL_PANE,
						mCallbacks.isDualPane())
				.putExtra(AccountDetailFragment.ARG_ACCOUNT_ID, accountID)
				.putExtra(AccountDetailFragment.ARG_TRANS_ID, transID);
		startActivity(intent);
	}

	void onCheckboxClicked(View v) {
		long taggedID = (Long) v.getTag();
		boolean isChecked = ((CheckBox) v).isChecked();
		VaultDatabaseHelper.getInstance(getActivity().getApplication())
				.setIfTransactionChecked(taggedID, isChecked);
	}

}
