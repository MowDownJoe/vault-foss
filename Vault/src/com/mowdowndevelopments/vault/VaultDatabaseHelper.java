package com.mowdowndevelopments.vault;

import java.util.Calendar;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.DatabaseErrorHandler;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

public class VaultDatabaseHelper extends SQLiteOpenHelper {

	// Transaction types:
	final static short DEPOSIT = 0, WITHDRAWAL = 1, CHECK = 2;
	// Database names:
	final static String TRANSTABLE = "Transactions", ACCTTABLE = "Accounts",
			TAGSTABLE = "Tags", TAGUSETABLE = "TagUsage",
			PARTYTABLE = "PartyPaidTo", REPEATSTABLE = "DelayedRepeats";
	// Tuple names:
	final static String ID = "ID", ACCTNAME = "AccountName", TAG = "TAG",
			TAGID = "TagID", TRANSID = "TransactionID", PARTYNAME = "Name",
			INITVALUE = "Initial_Value", DEFAULT = "IsDefault",
			LNKTRANSID = "Transaction_ID", DELAY = "Delay",
			REITERATE = "Continuous_Repost", DELAYMILLIS = "Delay_in_millis";
	final static String LNKACCTID = "Account_ID", CHECKNO = "Check_Number",
			DATEINMILLIS = "Date", TRANSVALUE = "Amount",
			PARTYLNK = "Party_ID", ISCHECKED = "Is_Checked",
			TRANSTYPE = "Transaction_Type", RUNTOTAL = "Running_Total",
			REPEATID = "Future_Repeat_ID", NOTIFY = "to_notify";
	final static String REPEATTYPE = "RepeatType", DELAYTYPE = "DelayType";
	// Other constants:
	final static int CURRENTDBVERSION = 5;
	/*
	 * Android requires a column named "_id" in any databases queries used to
	 * construct a list. Attempting to actually name a database column as such
	 * yields "ambiguous column name" errors with more complex Select
	 * statements. It's a hacky solution, but it works.
	 */
	final static String REALID = "_id";
	private Context context;
	private static VaultDatabaseHelper mInstance = null;

	public static VaultDatabaseHelper getInstance(Context ctx) {
		if (mInstance == null) {
			mInstance = new VaultDatabaseHelper(ctx.getApplicationContext());
		}
		return mInstance;

	}

	public VaultDatabaseHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
		this.context = context;
	}

	public VaultDatabaseHelper(Context context, String name,
			CursorFactory factory, int version,
			DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
		this.context = context;
	}

	public VaultDatabaseHelper(Context context) {
		super(context, "Vault", null, CURRENTDBVERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table if not exists " + ACCTTABLE + "(" + ID
				+ " integer primary key autoincrement, " + ACCTNAME + " text, "
				+ INITVALUE + " real, " + DEFAULT + " integer);");
		db.execSQL("create table if not exists " + TAGSTABLE + "(" + ID
				+ " integer primary key autoincrement, " + TAG
				+ " text unique);");
		db.execSQL("create table if not exists " + PARTYTABLE + "(" + ID
				+ " integer primary key autoincrement, " + PARTYNAME
				+ " text unique);"); // PARTY TABLE! WOOOO!
		db.execSQL("create table if not exists " + TRANSTABLE + "(" + ID
				+ " integer primary key autoincrement, " + LNKACCTID
				+ " integer, " + CHECKNO + " integer, " + DATEINMILLIS
				+ " integer, " + TRANSVALUE + " real, " + PARTYLNK
				+ " integer, " + ISCHECKED + " integer, " + TRANSTYPE
				+ " integer, " + "foreign key (" + LNKACCTID + ") references "
				+ ACCTTABLE + "(" + ID + ") on delete cascade);");
		db.execSQL("create table if not exists " + TAGUSETABLE + "(" + ID
				+ " integer primary key autoincrement, " + TAGID + " integer, "
				+ TRANSID + " integer, " + "foreign key (" + TRANSID
				+ ") references " + TRANSTABLE + "(" + ID
				+ ") on delete cascade, " + "foreign key (" + TAGID
				+ ") references " + TAGSTABLE + "(" + ID
				+ ") on delete cascade);");
		StringBuilder builder = new StringBuilder();
		builder.append("create table if not exists ").append(REPEATSTABLE)
				.append("(").append(ID)
				.append(" integer primary key autoincrement, ")
				.append(LNKTRANSID).append(" integer, ").append(NOTIFY)
				.append(" integer, ").append(DELAYMILLIS).append(" integer, ")
				.append(REPEATTYPE).append(" integer, ").append(DELAY)
				.append(" integer, ").append(DELAYTYPE)
				.append(" integer, foreign key(").append(LNKTRANSID)
				.append(") references ").append(TRANSTABLE).append("(")
				.append(ID).append(") on delete cascade);");
		db.execSQL(builder.toString());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (newVersion == 5) {
			db.execSQL(new StringBuilder().append("alter table ")
					.append(REPEATSTABLE).append(" add column ").append(NOTIFY)
					.append(" integer;").toString());
		}
		if (newVersion == 4) {
			db.execSQL(new StringBuilder().append("alter table ")
					.append(REPEATSTABLE).append(" add column ")
					.append(DELAYMILLIS).append(" integer;").toString());
		}
		if (oldVersion >= 1 && newVersion == 3) {
			db.execSQL("drop table if exists " + REPEATSTABLE + ";");
		}
		if (oldVersion >= 2 && newVersion == 3) {
			db.execSQL("drop table if exists " + TRANSTABLE + ";");
		}
		if (oldVersion != newVersion) {
			onCreate(db);
		}
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public void onConfigure(SQLiteDatabase db) {
		if (!db.isReadOnly()) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
				db.setForeignKeyConstraintsEnabled(true);
			} else {
				db.execSQL("PRAGMA foreign_keys = ON;");
			}
		}
	}

	protected long getMostRecentEntryID(String table, SQLiteDatabase readableDB)
			throws IllegalArgumentException {
		if (table.equalsIgnoreCase(ACCTTABLE)
				|| table.equalsIgnoreCase(TRANSTABLE)
				|| table.equalsIgnoreCase(PARTYTABLE)
				|| table.equalsIgnoreCase(REPEATSTABLE)
				|| table.equalsIgnoreCase(TAGSTABLE)
				|| table.equalsIgnoreCase(TAGUSETABLE)) {
			Cursor c = readableDB.rawQuery("select max(" + ID
					+ ") as max from " + table, null);
			if (c.moveToFirst()) {
				long entryID = c.getLong(c.getColumnIndexOrThrow("max"));
				c.close();
				return entryID;
			} else {
				c.close();
				throw new SQLException("Cursor was empty.");
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected long getMostRecentEntryID(String table)
			throws IllegalArgumentException {
		return getMostRecentEntryID(table, getReadableDatabase());
	}

	protected int getLastCheckWritten(SQLiteDatabase readableDB) {
		Cursor c = readableDB.rawQuery("select max(" + CHECKNO
				+ ") as max from " + TRANSTABLE, null);
		if (c.moveToFirst()) {
			int checkNo = c.getInt(c.getColumnIndexOrThrow("max"));
			c.close();
			return checkNo;
		} else {
			c.close();
			throw new SQLException("No checks could be found.");
		}
	}

	protected int getLastCheckWritten() {
		return getLastCheckWritten(getReadableDatabase());
	}

	protected int getLastCheckWritten(SQLiteDatabase readableDB, long accountID) {
		Cursor c = readableDB.rawQuery("select " + ID + ", " + LNKACCTID
				+ ", max(" + CHECKNO + ") as max from " + TRANSTABLE + "where "
				+ LNKACCTID + "=" + accountID, null);
		if (c.moveToFirst()) {
			int checkNo = c.getInt(c.getColumnIndexOrThrow("max"));
			c.close();
			return checkNo;
		} else {
			c.close();
			throw new SQLException("Cursor was empty.");
		}
	}

	protected int getLastCheckWritten(long accountID) {
		return getLastCheckWritten(getReadableDatabase(), accountID);
	}

	protected String getPartyName(long partyID, SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(PARTYTABLE, new String[] { ID, PARTYNAME },
				ID + "=" + partyID, null, null, null, null);
		if (c.moveToFirst()) {
			String partyName = c.getString(c.getColumnIndexOrThrow(PARTYNAME));
			c.close();
			return partyName;
		} else {
			c.close();
			throw new SQLException("Cursor was empty.");
		}
	}

	protected String getPartyName(long partyID) {
		return getPartyName(partyID, getReadableDatabase());
	}

	protected int getCheckNumber(long transactionID, SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(TRANSTABLE, new String[] { ID, CHECKNO },
				ID + "=" + transactionID, null, null, null, null);
		if (c.moveToFirst()) {
			int checkNo = c.getInt(c.getColumnIndexOrThrow(CHECKNO));
			c.close();
			return checkNo;
		} else {
			c.close();
			throw new SQLException("Cursor was empty.");
		}
	}

	protected int getCheckNumber(long transactionID) {
		return getCheckNumber(transactionID, getReadableDatabase());
	}

	protected double getOpeningBalance(long accountID, SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(ACCTTABLE, new String[] { ID, INITVALUE },
				ID + "=" + accountID, null, null, null, null);
		if (c.moveToFirst()) {
			double initBal = c.getDouble(c.getColumnIndexOrThrow(INITVALUE));
			c.close();
			return initBal;
		} else {
			c.close();
			throw new SQLException("Cursor was empty.");
		}
	}

	protected double getOpeningBalance(long accountID) {
		return getOpeningBalance(accountID, getReadableDatabase());
	}

	protected double getCurrentBalance(long accountID, SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(ACCTTABLE, new String[] { ID, INITVALUE },
				ID + "=" + accountID, null, null, null, null);
		double initBalance;
		if (c.moveToFirst()) {
			initBalance = c.getDouble(c.getColumnIndexOrThrow(INITVALUE));
		} else {
			// Will reach this else in rare instances where ListAdapter tries to
			// grab an account balance for an account that has been erased.
			initBalance = 0;
		}
		c = readableDB.rawQuery("select " + LNKACCTID + ", sum(" + TRANSVALUE
				+ ") as sum from " + TRANSTABLE + " where " + LNKACCTID + "="
				+ accountID, null);
		c.moveToFirst();
		double transactionTotal;
		try {
			transactionTotal = c.getDouble(c.getColumnIndexOrThrow("sum"));
		} catch (IllegalArgumentException e) {
			// Assumed line in try would throw if there was nothing to sum.
			transactionTotal = 0;
		} catch (CursorIndexOutOfBoundsException e) {
			transactionTotal = 0;
		}
		c.close();
		return initBalance + transactionTotal;
	}

	protected double getCurrentBalance(long accountID) {
		return getCurrentBalance(accountID, getReadableDatabase());
	}

	protected double getPostedBalance(long accountID, SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(ACCTTABLE, new String[] { ID, INITVALUE },
				ID + "=" + accountID, null, null, null, null);
		double initBalance;
		if (c.moveToFirst()) {
			initBalance = c.getDouble(c.getColumnIndexOrThrow(INITVALUE));
		} else {
			initBalance = 0;
		}
		c = readableDB.rawQuery("select " + LNKACCTID + ", sum(" + TRANSVALUE
				+ ") as sum from " + TRANSTABLE + " where " + LNKACCTID + "="
				+ accountID + " AND " + ISCHECKED + "=1", null);
		double postedTotal;
		if (c.moveToFirst()) {
			postedTotal = c.getDouble(c.getColumnIndexOrThrow("sum"));
		} else {
			postedTotal = 0;
		}
		c.close();
		return initBalance + postedTotal;
	}

	protected double getPostedBalance(long accountID) {
		return getPostedBalance(accountID, getReadableDatabase());
	}

	protected boolean isTransactionChecked(long accountID,
			SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(TRANSTABLE, new String[] { ID, ISCHECKED },
				ID + "=" + accountID, null, null, null, null);
		if (c.moveToFirst()) {
			boolean checked = (c.getShort(c.getColumnIndexOrThrow(ISCHECKED)) == 1);
			c.close();
			return checked;
		} else {
			c.close();
			throw new SQLException("Cursor was empty.");
		}
	}

	protected boolean isTransactionChecked(long accountID) {
		return isTransactionChecked(accountID, getReadableDatabase());
	}

	protected long createNewAccount(String accountName, double initBalance,
			boolean isDefault, SQLiteDatabase writableDB)
			throws IllegalArgumentException {
		if (writableDB.isReadOnly()) {
			throw new SQLiteException();
		}
		if (!(accountName.isEmpty() || accountName.equalsIgnoreCase("")
				|| accountName.contains(";") || accountName.contains(")")
				|| accountName.contains("--") || accountName.contains("\""))) {
			ContentValues values = new ContentValues();
			values.put(ACCTNAME, accountName);
			values.put(INITVALUE, initBalance);
			if (isDefault) {
				unsetAllDefaults(writableDB);
				values.put(DEFAULT, 1);

			} else {
				values.put(DEFAULT, 0);
			}
			writableDB.insertOrThrow(ACCTTABLE, null, values);
			return getMostRecentEntryID(ACCTTABLE, writableDB);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected long createNewAccount(String accountName, double initBalance,
			boolean isDefault) throws IllegalArgumentException {
		return createNewAccount(accountName, initBalance, isDefault,
				getWritableDatabase());
	}

	protected void unsetAllDefaults(SQLiteDatabase writableDB)
			throws IllegalArgumentException {
		if (!writableDB.isReadOnly()) {
			ContentValues values = new ContentValues();
			values.put(DEFAULT, 0);
			writableDB.update(ACCTTABLE, values, null, null);
			Log.d("Database after clearing defaults.",
					"There are "
							+ writableDB.query(ACCTTABLE,
									new String[] { ID, DEFAULT },
									DEFAULT + "=1", null, null, null, null)
									.getCount() + " defaults.");
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected void unsetAllDefaults() {
		unsetAllDefaults(getWritableDatabase());
	}

	protected String[] getTagsForTransaction(long transactionID,
			SQLiteDatabase readableDB) {
		Cursor c = readableDB.rawQuery("select t." + ID + " as " + ID + ", t."
				+ TAG + " as tag from " + TAGSTABLE + " as t, " + TAGUSETABLE
				+ " as u" + " where u." + TRANSID + "=t." + ID + " and t." + ID
				+ "=" + transactionID, null);
		c.moveToFirst();
		String[] tags = new String[c.getCount()];
		int length = tags.length;
		for (int i = 0; i < length; i++) {
			tags[i] = c.getString(c.getColumnIndexOrThrow("tag"));
			c.moveToNext();
		}
		return tags;
	}

	protected String[] getTagsForTransaction(long transactionID) {
		return getTagsForTransaction(transactionID, getReadableDatabase());
	}

	protected boolean setIfTransactionChecked(long transactionID,
			boolean isChecked, SQLiteDatabase writableDB)
			throws IllegalArgumentException {
		if (!writableDB.isReadOnly()) {
			ContentValues values = new ContentValues();
			if (isChecked) {
				values.put(ISCHECKED, 1);
			} else {
				values.put(ISCHECKED, 0);
			}
			return (writableDB.update(TRANSTABLE, values, ID + "="
					+ transactionID, null) == 1);
			// Since there should never be more than one entry with the same ID,
			// this should always return true.
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected boolean setIfTransactionChecked(long transactionID,
			boolean isChecked) throws IllegalArgumentException {
		return setIfTransactionChecked(transactionID, isChecked,
				getWritableDatabase());
	}

	protected Cursor getTags(SQLiteDatabase readableDB) {
		return readableDB.query(TAGSTABLE, new String[] { ID, TAG }, null,
				null, null, null, null);
	}

	protected Cursor getTags() {
		return getTags(getReadableDatabase());
	}

	protected String[] getTagsAsArray(SQLiteDatabase readableDB) {
		Cursor c = getTags(readableDB);
		String[] tags = new String[c.getCount()];
		int length = tags.length;
		Log.d("Test message", tags.length + "=" + c.getCount());
		c.moveToFirst();
		for (int i = 0; i < length; i++) {
			tags[i] = c.getString(c.getColumnIndexOrThrow(TAG));
			c.moveToNext();
		}
		return tags;
	}

	protected String[] getTagsAsArray() {
		return getTagsAsArray(getReadableDatabase());
	}

	protected Cursor getParties(SQLiteDatabase readableDB) {
		return readableDB.query(PARTYTABLE, new String[] { ID, PARTYNAME },
				null, null, null, null, null);
	}

	protected Cursor getParties() {
		return getParties(getReadableDatabase());
	}

	protected String[] getPartiesAsArray(SQLiteDatabase readableDB) {
		Cursor c = getParties(readableDB);
		String[] parties = new String[c.getCount()];
		int length = parties.length;
		Log.i("Test message", length + "=" + c.getCount());
		c.moveToFirst();
		for (int i = 0; i < length; i++) {
			parties[i] = c.getString(c.getColumnIndexOrThrow(PARTYNAME));
			c.moveToNext();
		}
		return parties;
	}

	protected String[] getPartiesAsArray() {
		return getPartiesAsArray(getReadableDatabase());
	}

	protected long getIDForTag(String tag, SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(TAGSTABLE, new String[] { ID, TAG }, TAG
				+ "=\"" + tag + "\"", null, null, null, null);
		if (c.getCount() != 0) {
			c.moveToFirst();
			long id = c.getLong(c.getColumnIndexOrThrow(ID));
			c.close();
			return id;
		} else {
			c.close();
			return -1;
		}
	}

	protected long getIDForTag(String tag) {
		return getIDForTag(tag, getReadableDatabase());
	}

	protected long getIDForParty(String party, SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(PARTYTABLE, new String[] { ID, PARTYNAME },
				PARTYNAME + "=\"" + party + "\"", null, null, null, null);
		if (c.getCount() != 0) {
			c.moveToFirst();
			long id = c.getLong(c.getColumnIndexOrThrow(ID));
			c.close();
			return id;
		} else {
			c.close();
			return -1;
		}
	}

	protected long getIDForParty(String party) {
		return getIDForParty(party, getReadableDatabase());
	}

	protected long newTag(String newTag, SQLiteDatabase writableDB)
			throws IllegalArgumentException {
		if (!writableDB.isReadOnly()) {
			ContentValues values = new ContentValues();
			values.put(TAG, newTag);
			return writableDB.insertOrThrow(TAGSTABLE, null, values);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected long newTag(String newTag) throws IllegalArgumentException {
		return newTag(newTag, getWritableDatabase());
	}

	protected long newParty(String party, SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			ContentValues values = new ContentValues();
			values.put(PARTYNAME, party);
			return writableDB.insertOrThrow(PARTYTABLE, null, values);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected long newParty(String party) {
		return newParty(party, getWritableDatabase());
	}

	protected long[] checkTagsIfExistThenCreate(String[] tags,
			SQLiteDatabase writableDB) throws IllegalArgumentException {
		if (!writableDB.isReadOnly()) {
			long[] createdTagIDs = new long[tags.length];
			long tagID;
			int noOfCreatedTags = 0, length = createdTagIDs.length;
			for (int i = 0; i < length; i++) {
				tagID = getIDForTag(tags[i], writableDB);
				if (tagID == -1) {
					tagID = newTag(tags[i], writableDB);
					Log.d("Checking tag batch...", "Adding tag " + tagID);
					Log.d("Checking tag batch...", "Tag "
							+ getMostRecentEntryID(TAGSTABLE, writableDB)
							+ " added.");
					++noOfCreatedTags;
				}
				createdTagIDs[i] = tagID;
			}
			Log.i("Tags created.", +noOfCreatedTags
					+ " tags have been added to the database.");
			return createdTagIDs;
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected long[] checkTagsIfExistThenCreate(String[] tags)
			throws IllegalArgumentException {
		return checkTagsIfExistThenCreate(tags, getWritableDatabase());
	}

	protected int clearTagUsageForTransaction(long transactionID,
			SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			return writableDB.delete(TAGUSETABLE,
					TRANSID + "=" + transactionID, null);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected int clearTagUsageForTransaction(long transactionID) {
		return clearTagUsageForTransaction(transactionID, getWritableDatabase());
	}

	public String createDashclockExtensionStatus(long accountID,
			SQLiteDatabase readableDB) {
		return Double.toString(getCurrentBalance(accountID, readableDB));
	}

	public String createDashclockExtensionStatus(long accountID) {
		return createDashclockExtensionStatus(accountID, getReadableDatabase());
	}

	public String createDashclockExtensionExpandedTitle(long accountID,
			SQLiteDatabase readableDB) {
		return new StringBuilder().append(context.getString(R.string.app_name))
				.append(context.getString(R.string.dashclockFluff))
				.append(getAccountName(accountID, readableDB)).append(':')
				.toString();
	}

	public String createDashclockExtensionExpandedTitle(long accountID) {
		return createDashclockExtensionExpandedTitle(accountID,
				getReadableDatabase());
	}

	public String createDashclockExtensionExpandedBody(long accountID) {
		return createDashclockExtensionExpandedBody(accountID,
				getReadableDatabase());
	}

	public String createDashclockExtensionExpandedBody(long accountID,
			SQLiteDatabase readableDB) {
		return new StringBuilder().append(context.getString(R.string.balance))
				.append(": ").append(getCurrentBalance(accountID, readableDB))
				.append("\n").append(context.getString(R.string.posted))
				.append(": ").append(getPostedBalance(accountID, readableDB))
				.toString();
	}

	protected String getAccountName(long id, SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(ACCTTABLE, new String[] { ID, ACCTNAME },
				ID + "=" + id, null, null, null, null);
		if (c.moveToFirst()) {
			String name = c.getString(c.getColumnIndex(ACCTNAME));
			c.close();
			return name;
		} else {
			c.close();
			throw new SQLException("Cursor was empty.");
		}
	}

	protected String getAccountName(long id) {
		return getAccountName(id, getReadableDatabase());
	}

	protected String getTransactionParty(long id, SQLiteDatabase readableDB) {
		StringBuilder builder = new StringBuilder();
		builder.append("select t.").append(ID).append(", p.").append(PARTYNAME)
				.append(" as party from ").append(TRANSTABLE).append(" as t, ")
				.append(PARTYTABLE).append(" as p, where t.").append(ID)
				.append("=").append(id).append(" and p.").append(ID)
				.append("=t.").append(PARTYLNK);
		Cursor c = readableDB.rawQuery(builder.toString(), null);
		if (c.moveToFirst()) {
			String party = c.getString(c.getColumnIndex("party"));
			c.close();
			return party;
		} else {
			c.close();
			throw new SQLException("Cursor was empty.");
		}
	}

	protected String getTransactionParty(long id) {
		return getTransactionParty(id, getReadableDatabase());
	}

	protected long getTransactionDate(long id, SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(TRANSTABLE,
				new String[] { ID, DATEINMILLIS }, ID + "=" + id, null, null,
				null, null);
		if (c.moveToFirst()) {
			long date = c.getLong(c.getColumnIndex(DATEINMILLIS));
			c.close();
			return date;
		} else {
			c.close();
			throw new SQLException("Cursor was empty.");
		}
	}

	protected long getTransactionDate(long id) {
		return getTransactionDate(id, getReadableDatabase());
	}

	protected double getTransactionValue(long id, SQLiteDatabase readableDB,
			boolean absoluteValue) {
		Cursor c = readableDB.query(TRANSTABLE,
				new String[] { ID, TRANSVALUE }, ID + "=" + id, null, null,
				null, null);
		if (c.moveToFirst()) {
			double value = c.getDouble(c.getColumnIndex(TRANSVALUE));
			c.close();
			if (absoluteValue) {
				return Math.abs(value);
			} else {
				return value;
			}
		} else {
			c.close();
			throw new SQLException();
		}
	}

	protected double getTransactionValue(long id, boolean absoluteValue) {
		return getTransactionValue(id, getReadableDatabase(), absoluteValue);
	}

	protected int getTransactionCheckNo(long id, SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(TRANSTABLE, new String[] { ID, TRANSTYPE,
				CHECKNO }, ID + "=" + id, null, null, null, null);
		c.moveToFirst();
		if (getTransactionType(id, readableDB) == CHECK) {
			int checkNo = c.getInt(c.getColumnIndex(CHECKNO));
			c.close();
			return checkNo;
		}
		c.close();
		return -1;
	}

	protected int getTransactionCheckNo(long id) {
		return getTransactionCheckNo(id, getReadableDatabase());
	}

	protected short getTransactionType(long id, SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(TRANSTABLE, new String[] { ID, TRANSTYPE },
				ID + "=" + id, null, null, null, null);
		if (c.moveToFirst()) {
			short type = c.getShort(c.getColumnIndex(TRANSTYPE));
			c.close();
			return type;
		} else {
			c.close();
			throw new SQLException();
		}
	}

	protected short getTransactionType(long id) {
		return getTransactionType(id, getReadableDatabase());
	}

	protected long newTransaction(SQLiteDatabase writableDB, long accountID,
			String party, int checkNo, long dateInMillis, double value,
			short transactionType) {
		if (!writableDB.isReadOnly()) {
			long partyID = getIDForParty(party, writableDB);
			if (partyID == -1) {
				partyID = newParty(party, writableDB);
			}
			ContentValues values = new ContentValues();
			values.put(LNKACCTID, accountID);
			values.put(TRANSTYPE, transactionType);
			values.put(DATEINMILLIS, dateInMillis);
			values.put(PARTYLNK, partyID);
			if (transactionType == DEPOSIT) {
				values.put(TRANSVALUE, value);
			} else {
				values.put(TRANSVALUE, -value);
			}
			values.put(ISCHECKED, 0);
			// values.put(RUNTOTAL, (getCurrentBalance(accountID, writableDB) +
			// value));
			if (transactionType == CHECK) {
				values.put(CHECKNO, checkNo);
			}
			return writableDB.insertOrThrow(TRANSTABLE, null, values);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected long newTransaction(long accountID, String party, int checkNo,
			long dateInMillis, double value, short transactionType) {
		return newTransaction(getWritableDatabase(), accountID, party, checkNo,
				dateInMillis, value, transactionType);
	}

	protected boolean updateTransaction(SQLiteDatabase writableDB,
			long transactionID, String party, int checkNo, long dateInMillis,
			double value, short transactionType) {
		if (!writableDB.isReadOnly()) {
			long partyID = getIDForParty(party, writableDB);
			if (partyID == -1) {
				partyID = newParty(party, writableDB);
			}
			ContentValues values = new ContentValues();
			values.put(TRANSTYPE, transactionType);
			values.put(DATEINMILLIS, dateInMillis);
			values.put(PARTYLNK, partyID);
			if (transactionType == 0) {
				values.put(TRANSVALUE, value);
			} else {
				values.put(TRANSVALUE, -value);
			}
			if (transactionType == CHECK) {
				values.put(CHECKNO, checkNo);
			}
			return (writableDB.update(TRANSTABLE, values, ID + "="
					+ transactionID, null) == 1);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected boolean updateTransaction(long transactionID, String party,
			int checkNo, long dateInMillis, double value, short transactionType) {
		return updateTransaction(getWritableDatabase(), transactionID, party,
				checkNo, dateInMillis, value, transactionType);
	}

	protected void associateTagsWithTransaction(SQLiteDatabase writableDB,
			long[] tags, long transactionID) {
		if (!writableDB.isReadOnly()) {
			writableDB.delete(TAGUSETABLE, TRANSID + "=" + transactionID, null);
			int length = tags.length;
			ContentValues values = new ContentValues();
			for (int i = 0; i < length; i++) {
				values.put(TAGID, tags[i]);
				values.put(TRANSID, transactionID);
				writableDB.insertOrThrow(TAGUSETABLE, null, values);
				values.clear();
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected void associateTagsWithTransaction(long[] tags, long transactionID) {
		associateTagsWithTransaction(getWritableDatabase(), tags, transactionID);
	}

	protected void associateTagsWithTransaction(SQLiteDatabase writableDB,
			String[] tags, long transactionID) throws IllegalArgumentException {
		long[] tagIDs = checkTagsIfExistThenCreate(tags, writableDB);
		associateTagsWithTransaction(writableDB, tagIDs, transactionID);
	}

	protected void associateTagsWithTransaction(String[] tags,
			long transactionID) throws IllegalArgumentException {
		associateTagsWithTransaction(getWritableDatabase(), tags, transactionID);
	}

	protected long getDefaultAccount(SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(ACCTTABLE, new String[] { ID, DEFAULT },
				DEFAULT + "=1", null, null, null, null);
		if (c.getCount() == 0) {
			return -1;
		} else if (c.getCount() == 1) {
			c.moveToFirst();
			long accountID = c.getLong(c.getColumnIndex(ID));
			c.close();
			return accountID;
		} else {
			c.close();
			throw new SQLException("Somehow, multiple defaults have been set.");
		}
	}

	protected long getDefaultAccount() {
		return getDefaultAccount(getReadableDatabase());
	}

	protected boolean isAccountDefault(long accountID, SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(ACCTTABLE, new String[] { ID, DEFAULT }, ID
				+ "=" + accountID, null, null, null, null);
		if (c.moveToFirst()) {
			boolean isDefault = (c.getShort(c.getColumnIndexOrThrow(DEFAULT)) == 1);
			c.close();
			return isDefault;
		} else {
			c.close();
			throw new SQLException();
		}
	}

	protected boolean isAccountDefault(long accountID) {
		return isAccountDefault(accountID, getReadableDatabase());
	}

	protected int setNewDefault(long accountID, SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			ContentValues values = new ContentValues();
			values.put(DEFAULT, 1);
			unsetAllDefaults(writableDB);
			return writableDB.update(ACCTTABLE, values, ID + "=" + accountID,
					null);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected int setNewDefault(long accountID) {
		return setNewDefault(accountID, getWritableDatabase());
	}

	/**
	 * @param accountID
	 * @param writableDB
	 * @return true if something was set as default. False if default was clear.
	 */
	protected boolean toggleIfDefault(long accountID, SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			if (isAccountDefault(accountID, writableDB)) {
				unsetAllDefaults(writableDB);
				return false;
			} else {
				setNewDefault(accountID, writableDB);
				return true;
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * @param accountID
	 * @return true if something was set as default. False if default was clear.
	 */
	protected boolean toggleIfDefault(long accountID) {
		return toggleIfDefault(accountID, getWritableDatabase());
	}

	protected int deleteTransaction(long transID, SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			return writableDB.delete(TRANSTABLE, ID + "=" + transID, null);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected int deleteTransaction(long transID) {
		return deleteTransaction(transID, getWritableDatabase());
	}

	protected void deleteTransactions(long[] transIDs, SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			int sizeOf = transIDs.length;
			for (int i = 0; i < sizeOf; i++) {
				deleteTransaction(transIDs[i], writableDB);
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected void deleteTransactions(long[] transIDs) {
		deleteTransactions(transIDs, getWritableDatabase());
	}

	protected int deleteAccount(long accountID, SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			return writableDB.delete(ACCTTABLE, ID + "=" + accountID, null);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected int deleteAccount(long accountID) {
		return deleteAccount(accountID, getWritableDatabase());
	}

	protected void deleteAccounts(long[] accountIDs, SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			int sizeOf = accountIDs.length;
			for (int i = 0; i < sizeOf; i++) {
				deleteTransaction(accountIDs[i], writableDB);
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected void deleteAccounts(long[] accountIDs) {
		deleteAccounts(accountIDs, getWritableDatabase());
	}

	protected void deleteAccounts(Long[] accountIDs, SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			int sizeOf = accountIDs.length;
			for (int i = 0; i < sizeOf; i++) {
				deleteTransaction(accountIDs[i], writableDB);
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected void deleteAccounts(Long[] accountIDs) {
		deleteAccounts(accountIDs, getWritableDatabase());
	}

	protected long replicateTransaction(SQLiteDatabase writableDB,
			long transactionID, long delay) {
		if (!writableDB.isReadOnly()) {
			Cursor c = writableDB.query(TRANSTABLE, new String[] { ID,
					LNKACCTID, TRANSTYPE, DATEINMILLIS, PARTYLNK, TRANSVALUE,
					ISCHECKED }, ID + "=" + transactionID, null, null, null,
					null);
			ContentValues values = new ContentValues();
			c.moveToFirst();
			Calendar cal = Calendar.getInstance();
			long accountID = c.getLong(c.getColumnIndexOrThrow(LNKACCTID));
			double value = c.getDouble(c.getColumnIndexOrThrow(TRANSVALUE));
			short type = c.getShort(c.getColumnIndexOrThrow(TRANSTYPE));
			values.put(LNKACCTID, accountID);
			values.put(PARTYLNK, c.getLong(c.getColumnIndexOrThrow(PARTYLNK)));
			values.put(TRANSVALUE, value);
			values.put(TRANSTYPE, type);
			if (type == CHECK) {
				values.put(CHECKNO,
						getLastCheckWritten(writableDB, accountID) + 1);
			}
			values.put(DATEINMILLIS, cal.getTimeInMillis() + delay);
			values.put(ISCHECKED, 0);
			long newTransID = writableDB
					.insertOrThrow(TRANSTABLE, null, values);
			String[] tags = getTagsForTransaction(transactionID, writableDB);
			if (tags.length != 0) {
				Intent intent = new Intent(context, TagBinderService.class)
						.putExtra(AccountDetailFragment.ARG_TRANS_ID,
								newTransID).putExtra(TagBinderService.ARG_TAGS,
								tags);
				context.startService(intent);
			}
			return newTransID;
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected long replicateTransaction(long transactionID, long delay) {
		return replicateTransaction(getWritableDatabase(), transactionID, delay);
	}

	protected Cursor getPartyNameQueryResults(SQLiteDatabase readableDB,
			String query) {
		return readableDB.query(PARTYTABLE, new String[] { ID, PARTYNAME },
				PARTYNAME + "=\"" + query + "\"", null, null, null, PARTYNAME);
	}

	protected Cursor getPartyNameQueryResults(String query) {
		return getPartyNameQueryResults(getReadableDatabase(), query);
	}

	protected boolean renameAccount(String name, long accountID,
			SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			ContentValues values = new ContentValues();
			values.put(ACCTNAME, name);
			return (writableDB.update(ACCTTABLE, values, ID + "=" + accountID,
					null) == 1);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected boolean renameAccount(String name, long accountID) {
		return renameAccount(name, accountID, getWritableDatabase());
	}

	protected boolean updateOpeningBalance(double balance, long accountID,
			SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			ContentValues values = new ContentValues();
			values.put(INITVALUE, balance);
			return (writableDB.update(ACCTTABLE, values, ID + "=" + accountID,
					null) == 1);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected boolean updateOpeningBalance(double balance, long accountID) {
		return updateOpeningBalance(balance, accountID, getWritableDatabase());
	}

//	protected Cursor getSearchResultForTag(long accountID, String query,
//			SQLiteDatabase readableDB) {
//		return null;
//	}
//
//	protected Cursor getSearchResultForTag(long accountID, String query) {
//		return getSearchResultForTag(accountID, query, getReadableDatabase());
//	}

	protected long getTransactionTimeInMillis(long accountID,
			SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(TRANSTABLE,
				new String[] { ID, DATEINMILLIS }, ID + "=" + accountID, null,
				null, null, null);
		if (c.moveToFirst()) {
			long date = c.getLong(c.getColumnIndexOrThrow(DATEINMILLIS));
			c.close();
			return date;
		} else {
			c.close();
			throw new SQLException();
		}

	}

	protected long getTransactionTimeInMillis(long accountID) {
		return getTransactionTimeInMillis(accountID, getReadableDatabase());
	}

	protected long getRepeatTransactionDelay(long accountID,
			SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(REPEATSTABLE, new String[] { ID,
				LNKTRANSID, DELAYMILLIS }, LNKTRANSID + "=" + accountID, null,
				null, null, null);
		if (c.moveToFirst()) {
			long delay = c.getLong(c.getColumnIndexOrThrow(DELAYMILLIS));
			c.close();
			return delay;
		} else {
			c.close();
			throw new SQLException();
		}

	}

	protected long getRepeatTransactionDelay(long accountID) {
		return getRepeatTransactionDelay(accountID, getReadableDatabase());
	}

	protected Long[] getRepeatingTransactions(SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(REPEATSTABLE,
				new String[] { ID, LNKTRANSID }, null, null, null, null, null);
		Long[] ids = new Long[c.getCount()];
		int length = ids.length;
		if (length != 0) {
			c.moveToFirst();
			for (int i = 0; i < length; i++) {
				ids[0] = c.getLong(c.getColumnIndexOrThrow(LNKTRANSID));
				c.moveToNext();
			}
			return ids;
		} else {
			return null;
		}
	}

	protected Long[] getRepeatingTransactions() {
		return getRepeatingTransactions(getReadableDatabase());
	}

	protected long getIDForTagLikeQuery(String query, SQLiteDatabase readableDB) {
		Cursor c = readableDB.rawQuery(
				new StringBuilder().append("select * from ").append(TAGSTABLE)
						.append(" where ").append(TAG).append(" LIKE ?")
						.toString(), new String[] { query.trim() });
		if (c.moveToFirst()) {
			long id = c.getLong(c.getColumnIndexOrThrow(ID));
			c.close();
			return id;
		} else {
			c.close();
			return -1;
		}
	}

	protected long getIDForTagLikeQuery(String query) {
		return getIDForTagLikeQuery(query, getReadableDatabase());
	}

	protected long getPartyIDForTransaction(long transactionID,
			SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(TRANSTABLE, new String[] { ID, PARTYLNK },
				ID + "=" + transactionID, null, null, null, null);
		if (c.moveToFirst()) {
			long id = c.getLong(c.getColumnIndexOrThrow(PARTYLNK));
			c.close();
			return id;
		} else {
			c.close();
			throw new IllegalArgumentException();
		}
	}

	protected long getPartyIDForTransaction(long transactionID) {
		return getPartyIDForTransaction(transactionID, getReadableDatabase());
	}

	protected String getPartyForTransaction(long transactionID,
			SQLiteDatabase readableDB) {
		return getPartyName(
				getPartyIDForTransaction(transactionID, readableDB), readableDB);
	}

	protected String getPartyForTransaction(long transactionID) {
		return getPartyForTransaction(transactionID, getReadableDatabase());
	}

	protected boolean doesRepeatingTransactionNotify(long transactionID,
			SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(REPEATSTABLE, new String[] { ID,
				LNKTRANSID, NOTIFY }, LNKTRANSID + "=" + transactionID, null,
				null, null, null);
		if (c.moveToFirst()) {
			boolean doesItNotify = (c.getInt(c.getColumnIndexOrThrow(NOTIFY)) == 1);
			c.close();
			return doesItNotify;
		} else {
			c.close();
			throw new IllegalArgumentException();
		}
	}

	protected boolean doesRepeatingTransactionNotify(long transactionID) {
		return doesRepeatingTransactionNotify(transactionID,
				getReadableDatabase());
	}

	protected long setTransactionRepetition(long transactionID,
			boolean shouldNotify, int repeatMode, int delayTime, int delayType,
			long derivedDelayTimeMillis, SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			ContentValues values = new ContentValues();
			values.put(LNKTRANSID, transactionID);
			values.put(DELAYMILLIS, derivedDelayTimeMillis);
			if (shouldNotify) {
				values.put(NOTIFY, 1);
			} else {
				values.put(NOTIFY, 0);
			}
			values.put(REPEATTYPE, repeatMode);
			values.put(DELAY, delayTime);
			values.put(DELAYTYPE, delayType);
			return writableDB.insertOrThrow(REPEATSTABLE, null, values);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected long setTransactionRepetition(long transactionID,
			boolean shouldNotify, int repeatMode, int delayTime, int delayType,
			long derivedDelayTimeMillis) {
		return setTransactionRepetition(transactionID, shouldNotify,
				repeatMode, delayTime, delayType, derivedDelayTimeMillis,
				getWritableDatabase());
	}

	protected int unsetTransactionRepetition(long transactionID,
			SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			return writableDB.delete(REPEATSTABLE, LNKTRANSID + "="
					+ transactionID, null);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected int unsetTransactionRepetition(long transactionID) {
		return unsetTransactionRepetition(transactionID, getWritableDatabase());
	}

	protected int editTransactionRepetition(long transactionID,
			boolean shouldNotify, int repeatMode, int delayTime, int delayType,
			long derivedDelayTimeMillis, SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			ContentValues values = new ContentValues();
			values.put(DELAYMILLIS, derivedDelayTimeMillis);
			if (shouldNotify) {
				values.put(NOTIFY, 1);
			} else {
				values.put(NOTIFY, 0);
			}
			values.put(REPEATTYPE, repeatMode);
			values.put(DELAY, delayTime);
			values.put(DELAYTYPE, delayType);
			return writableDB.update(REPEATSTABLE, values, LNKTRANSID + "="
					+ transactionID, null);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected int editTransactionRepetition(long transactionID,
			boolean shouldNotify, int repeatMode, int delayTime, int delayType,
			long derivedDelayTimeMillis) {
		return editTransactionRepetition(transactionID, shouldNotify,
				repeatMode, delayTime, delayType, derivedDelayTimeMillis,
				getWritableDatabase());
	}

	protected Cursor getRepeatingTransactionData(long transactionID,
			SQLiteDatabase readableDB) {
		return readableDB.query(REPEATSTABLE, null, LNKTRANSID + "="
				+ transactionID, null, null, null, null);
	}

	protected Cursor getRepeatingTransactionData(long transactionID) {
		return getRepeatingTransactionData(transactionID, getReadableDatabase());
	}

	protected boolean doesTransactionRepeat(long transactionID,
			SQLiteDatabase readableDB) {
		Cursor c = readableDB.query(REPEATSTABLE,
				new String[] { ID, LNKTRANSID }, LNKTRANSID + "="
						+ transactionID, null, null, null, null);
		boolean doesItExist = c.moveToFirst();
		c.close();
		return doesItExist;
	}

	protected boolean doesTransactionRepeat(long transactionID) {
		return doesTransactionRepeat(transactionID, getReadableDatabase());
	}

	protected int reassignRepeat(long repeatID, long transactionID,
			SQLiteDatabase writableDB) {
		if (!writableDB.isReadOnly()) {
			ContentValues values = new ContentValues();
			values.put(LNKTRANSID, transactionID);
			return writableDB.update(REPEATSTABLE, values, ID + "=" + repeatID,
					null);
		} else {
			throw new IllegalArgumentException();
		}
	}

	protected int reassignRepeat(long repeatID, long transactionID) {
		return reassignRepeat(repeatID, transactionID, getWritableDatabase());
	}

}
