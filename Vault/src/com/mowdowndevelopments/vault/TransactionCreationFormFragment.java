package com.mowdowndevelopments.vault;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;

import com.androidquery.AQuery;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link TransactionCreationFormFragment.Callbacks} interface to handle
 * interaction events.
 * 
 */
public class TransactionCreationFormFragment extends Fragment implements
		AdapterView.OnItemSelectedListener {

	private Callbacks mListener;
	ArrayAdapter<String> adapter;
	LinkedList<String> list;
	private AQuery aq;
	private Calendar cal;
	private EditText amountBox, checkNoBox, dateBox;
	private AutoCompleteTextView partyBox;
	private Button repeatButton;
	private Spinner transTypeSpinner;
	private short transactionType = 1;

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface Callbacks {

		long getTransactionID();

		void setTransactionID(long transID);

		long getAccountID();

		void setAccountID(long acctID);

		void onRepetitionTypeSet();
	}

	public TransactionCreationFormFragment() {
		// Required empty public constructor
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (Callbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cal = Calendar.getInstance();
		list = new LinkedList<String>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_transaction_creation_form,
				container, false);
	}

	@Override
	public void onViewCreated(View v, Bundle savedInstanceState) {
		super.onViewCreated(v, savedInstanceState);
		long transID = getActivity().getIntent().getLongExtra(
				AccountDetailFragment.ARG_TRANS_ID, -1);
		aq = new AQuery(getActivity(), v);
		adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line,
				android.R.id.text1, list);
		partyBox = (AutoCompleteTextView) v.findViewById(R.id.partyBox);
		partyBox.setAdapter(adapter);
		transTypeSpinner = (Spinner) v.findViewById(R.id.transactionSpinner);
		transTypeSpinner.setOnItemSelectedListener(this);
		if (transID != -1) {
			Cursor c = VaultDatabaseHelper.getInstance(getActivity())
					.getRepeatingTransactionData(transID);
			if (c.moveToFirst()) {
				setRepeatButtonText(
						c.getInt(c
								.getColumnIndexOrThrow(VaultDatabaseHelper.REPEATTYPE)),
						c.getInt(c
								.getColumnIndexOrThrow(VaultDatabaseHelper.DELAY)),
						c.getInt(c
								.getColumnIndexOrThrow(VaultDatabaseHelper.DELAYTYPE)));
			}
			c.close();
			transactionType = VaultDatabaseHelper.getInstance(
					getActivity().getApplication()).getTransactionType(transID);
			amountBox = aq
					.id(R.id.amountBox)
					.text(""
							+ VaultDatabaseHelper.getInstance(
									getActivity().getApplication())
									.getTransactionValue(transID, true))
					.getEditText();
			dateBox = aq
					.id(R.id.dateInputBox)
					.text(DateUtils.formatDateTime(
							getActivity(),
							VaultDatabaseHelper.getInstance(
									getActivity().getApplication())
									.getTransactionDate(transID),
							DateUtils.FORMAT_NUMERIC_DATE)).getEditText();

			partyBox.setText(VaultDatabaseHelper.getInstance(
					getActivity().getApplication()).getPartyForTransaction(
					transID));
			if (transactionType == VaultDatabaseHelper.CHECK) {
				checkNoBox = aq
						.id(R.id.checkNumberBox)
						.visible()
						.text(""
								+ VaultDatabaseHelper.getInstance(
										getActivity().getApplication())
										.getCheckNumber(transID)).getEditText();
			} else {
				checkNoBox = (EditText) v.findViewById(R.id.checkNumberBox);
			}
		} else {
			dateBox = aq
					.id(R.id.dateInputBox)
					.text(DateUtils.formatDateTime(getActivity(),
							cal.getTimeInMillis(),
							DateUtils.FORMAT_NUMERIC_DATE)).getEditText();
			amountBox = (EditText) v.findViewById(R.id.amountBox);
			checkNoBox = (EditText) v.findViewById(R.id.checkNumberBox);
		}
		transTypeSpinner.setSelection(transactionType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Fragment#onActivityCreated(android.os.Bundle)
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		new PartySuggestionsBuilder(getActivity()).execute();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	public void onButtonPressed() {
		if (mListener != null) {
			mListener.onRepetitionTypeSet();
		}
	}

	/**
	 * @return the amountBox's text, parsed as a Double
	 * @throws ParseException 
	 */
	protected double getAmountBoxValue() throws ParseException {
		String value = amountBox.getText().toString().trim();
		if (value.contains(";") || value.contains(")") || value.contains("--")
				|| value.contains("\'")) {
			IllegalArgumentException e = new IllegalArgumentException();
			Log.e("TransactionForm", "Bad value input", e);
			throw e;
		} else if (!(value.isEmpty() || value.equalsIgnoreCase(""))) {
			NumberFormat formatter = NumberFormat.getInstance();
			formatter.setMaximumFractionDigits(2);
			return formatter.parse(value).doubleValue();
		} else {
			return 0;
		}
	}

	/**
	 * @return the checkNoBox's text, parsed as an Integer
	 */
	protected int getCheckNoBoxValue() {
		String value = checkNoBox.getText().toString().trim();
		boolean badValue = (value.isEmpty() || value.equalsIgnoreCase(""));
		if (value.contains(";") || value.contains(")") || value.contains("--")
				|| value.contains("\"")) {
			IllegalArgumentException e = new IllegalArgumentException();
			Log.e("TransactionForm", "Bad check number input", e);
			throw e;
		} else if (transactionType == VaultDatabaseHelper.CHECK && !badValue) {
			return Integer.parseInt(checkNoBox.getText().toString());
		} else if (badValue && (transactionType == VaultDatabaseHelper.CHECK)) {
			throw new IllegalArgumentException(
					"A check number has not been entered.");
		} else {
			return -1;
		}
	}

	/**
	 * @return the partyBox's text
	 */
	protected String getPartyBoxInput() {
		String nameInput = partyBox.getText().toString().trim();
		if (!(nameInput.contains(";") || nameInput.contains(")")
				|| nameInput.contains("--") || nameInput.contains("\""))) {
			return nameInput;
		} else {
			IllegalArgumentException e = new IllegalArgumentException();
			Log.e("TransactionForm", "Bad party input", e);
			throw e;
		}
	}

	/**
	 * @return the transactionType
	 */
	protected short getTransactionType() {
		return transactionType;
	}

	/**
	 * 
	 */
	protected void setDate(int month, int day, int year) {
		cal.set(year, month, day);
		dateBox.setText(DateUtils.formatDateTime(getActivity(),
				cal.getTimeInMillis(), DateUtils.FORMAT_NUMERIC_DATE));
	}

	protected Calendar getDate() {
		return cal;
	}

	/**
	 * 
	 */
	protected void setRepeatButtonText(int repeatType, int delayValue,
			int delayType) {
		switch (repeatType) {
		case 0:
			repeatButton.setText(R.string.noRepeat);
			break;
		case 1:
			StringBuilder builder = new StringBuilder()
					.append(getActivity().getString(R.string.every))
					.append(' ').append(delayValue).append(' ');
			String[] timeFrames = getResources().getStringArray(
					R.array.timeframes);
			builder.append(timeFrames[delayType]);
			repeatButton.setText(builder.toString());
			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> spinner, View v, int position,
			long id) {
		Integer temp = position;
		transactionType = temp.shortValue();
		if (position == 2) {
			checkNoBox.setVisibility(View.VISIBLE);
		} else {
			checkNoBox.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		Log.d("TransactionFormFragment", "Nothing selected");
	}

	public class PartySuggestionsBuilder extends AsyncTask<Void, Void, Void> {

		Context context;
		String[] parties;

		public PartySuggestionsBuilder(Context context) {
			super();
			this.context = context;
		}

		@Override
		protected Void doInBackground(Void... params) {
			parties = VaultDatabaseHelper.getInstance(context)
					.getPartiesAsArray();
			Log.d("TagListBuilderBase", "Finished building array of parties.");
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (!isRemoving() || !isDetached() || !getActivity().isFinishing()) {
				list.addAll(Arrays.asList(parties));
				adapter.notifyDataSetChanged();
			}
		}

	}

}
