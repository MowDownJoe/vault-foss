package com.mowdowndevelopments.vault;

import java.text.NumberFormat;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

public class TransactionListCursorAdapter extends ResourceCursorAdapter {

	final static String LOADEREX_INIT_QUERY = "select t."
			+ VaultDatabaseHelper.ID + " as " + VaultDatabaseHelper.REALID
			+ ", t." + VaultDatabaseHelper.ISCHECKED + " as "
			+ VaultDatabaseHelper.ISCHECKED + ", t."
			+ VaultDatabaseHelper.DATEINMILLIS + " as "
			+ VaultDatabaseHelper.DATEINMILLIS + ", p."
			+ VaultDatabaseHelper.PARTYNAME + " as "
			+ VaultDatabaseHelper.PARTYNAME + ", t."
			+ VaultDatabaseHelper.TRANSVALUE + " as "
			+ VaultDatabaseHelper.TRANSVALUE + ", t."
			+ VaultDatabaseHelper.TRANSTYPE + " as "
			+ VaultDatabaseHelper.TRANSTYPE + ", t."
			+ VaultDatabaseHelper.CHECKNO + " as "
			+ VaultDatabaseHelper.CHECKNO + " from "
			+ VaultDatabaseHelper.TRANSTABLE + " as t, "
			+ VaultDatabaseHelper.PARTYTABLE + " as p" + " where p."
			+ VaultDatabaseHelper.ID + "=t." + VaultDatabaseHelper.PARTYLNK
			+ " and t." + VaultDatabaseHelper.LNKACCTID + "=?"; 
			//Loader will replace ? with relevant ID.
	
	final static String ALT_LOADEREX_INIT_QUERY = "select "
			+ VaultDatabaseHelper.ID + " as " + VaultDatabaseHelper.REALID
			+ ", " + VaultDatabaseHelper.ISCHECKED + ", "
			+ VaultDatabaseHelper.DATEINMILLIS + ", "
			+ VaultDatabaseHelper.PARTYLNK + ", "
			+ VaultDatabaseHelper.TRANSVALUE + ", "
			+ VaultDatabaseHelper.TRANSTYPE + ", "
			+ VaultDatabaseHelper.CHECKNO + " from "
			+ VaultDatabaseHelper.TRANSTABLE + " where "
			+ VaultDatabaseHelper.LNKACCTID + "= ?"; //Not used.

	int layout = 0;

	public TransactionListCursorAdapter(Context context, int layout, Cursor c,
			boolean autoRequery) {
		super(context, layout, c, autoRequery);
		this.layout = layout;
	}

	public TransactionListCursorAdapter(Context context, int layout, Cursor c,
			int flags) {
		super(context, layout, c, flags);
		this.layout = layout;
	}

	public TransactionListCursorAdapter(Context context, Cursor c) {
		super(context, R.layout.transaction_list_item, c, false);
		this.layout = R.layout.transaction_list_item;
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(layout, parent, false);
		//When you're targeting 4.0 and later, View.setTag() does all the work of ViewHolder with less fuss.
		v.setTag(R.id.transactionCheckBox,
				v.findViewById(R.id.transactionCheckBox));
		v.setTag(R.id.transactionDateView, v.findViewById(R.id.transactionDateView));
		v.setTag(R.id.transactionNameView, v.findViewById(R.id.transactionNameView));
		v.setTag(R.id.transactionValueView, v.findViewById(R.id.transactionValueView));
		v.setTag(R.id.transactionTypeView, v.findViewById(R.id.transactionTypeView));
		return v;
	}

	@Override
	public void bindView(View v, Context context, Cursor c) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		boolean toColor = prefs.getBoolean("shouldColorBackgrounds", false);
		CheckBox box = (CheckBox) v.getTag(R.id.transactionCheckBox);
		TextView dateView = (TextView) v.getTag(R.id.transactionDateView);
		TextView partyView = (TextView) v.getTag(R.id.transactionNameView);
		TextView typeView = (TextView) v.getTag(R.id.transactionTypeView);
		TextView valueView = (TextView) v.getTag(R.id.transactionValueView);
		box.setTag(c.getLong(c
				.getColumnIndexOrThrow(VaultDatabaseHelper.REALID)));
		box.setChecked(c.getShort(c
				.getColumnIndexOrThrow(VaultDatabaseHelper.ISCHECKED)) == 1);
		dateView.setText(DateUtils.formatDateTime(context, c.getLong(c
				.getColumnIndexOrThrow(VaultDatabaseHelper.DATEINMILLIS)),
				DateUtils.FORMAT_NUMERIC_DATE));
		partyView.setText(c.getString(c
				.getColumnIndexOrThrow(VaultDatabaseHelper.PARTYNAME)));
		/*
		 * partyView .setText(VaultDatabaseHelper .getInstance(context)
		 * .getPartyName( c.getLong(c
		 * .getColumnIndexOrThrow(VaultDatabaseHelper.PARTYLNK)))); //For use
		 * with alt init statement.
		 */
		valueView
				.setText(NumberFormat
						.getCurrencyInstance()
						.format(Math.abs(c.getDouble(c
								.getColumnIndexOrThrow(VaultDatabaseHelper.TRANSVALUE)))));
		switch (c
				.getInt(c.getColumnIndexOrThrow(VaultDatabaseHelper.TRANSTYPE))) {
		case VaultDatabaseHelper.DEPOSIT:
			typeView.setText(R.string.deposit);
			if (toColor) {
				dateView.setTextColor(prefs.getInt("depositColor",
						Color.argb(255, 0, 0, 255)));
				partyView.setTextColor(prefs.getInt("depositColor",
						Color.argb(255, 0, 0, 255)));
				valueView.setTextColor(prefs.getInt("depositColor",
						Color.argb(255, 0, 0, 255)));
				typeView.setTextColor(prefs.getInt("depositColor",
						Color.argb(255, 0, 0, 255)));
			}
			break;
		case VaultDatabaseHelper.WITHDRAWAL:
			typeView.setText(R.string.withdrawal);
			if (toColor) {
				dateView.setTextColor(prefs.getInt("withdrawColor",
						Color.argb(255, 255, 0, 0)));
				partyView.setTextColor(prefs.getInt("withdrawColor",
						Color.argb(255, 255, 0, 0)));
				valueView.setTextColor(prefs.getInt("withdrawColor",
						Color.argb(255, 255, 0, 0)));
				typeView.setTextColor(prefs.getInt("withdrawColor",
						Color.argb(255, 255, 0, 0)));
			}
			break;
		case VaultDatabaseHelper.CHECK:
			typeView.setText(new StringBuilder()
					.append(context.getString(R.string.check))
					.append(c.getInt(c.getColumnIndexOrThrow(VaultDatabaseHelper.CHECKNO)))
					.toString());
			if (toColor) {
				dateView.setTextColor(prefs.getInt("checkColor",
						Color.argb(255, 0, 255, 0)));
				partyView.setTextColor(prefs.getInt("checkColor",
						Color.argb(255, 0, 255, 0)));
				valueView.setTextColor(prefs.getInt("checkColor",
						Color.argb(255, 0, 255, 0)));
				typeView.setTextColor(prefs.getInt("checkColor",
						Color.argb(255, 0, 255, 0)));
				v.setBackgroundColor(prefs.getInt("checkColor",
						Color.argb(204, 0, 255, 0)));
			}
			break;
		default:
			typeView.setText("ERROR");
		} 

	}

}
