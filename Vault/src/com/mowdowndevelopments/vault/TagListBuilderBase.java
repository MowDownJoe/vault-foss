package com.mowdowndevelopments.vault;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class TagListBuilderBase extends AsyncTask<Void, Void, Void> {

	Context context;
	Long transID = null;
	String[] tags;
	public TagListBuilderBase(Context context, Long id) {
		super();
		this.context = context;
		this.transID = id;
	}

	public TagListBuilderBase(Context context) {
		super();
		this.context = context;
	}

	@Override
	protected Void doInBackground(Void... params) {
		if (transID==null){
			tags = VaultDatabaseHelper.getInstance(context).getTagsAsArray();
		} else {
			tags = VaultDatabaseHelper.getInstance(context).getTagsForTransaction(transID);
		}
		Log.d("TagListBuilderBase", "Finished building array of tags.");
		return null;
	}

}
