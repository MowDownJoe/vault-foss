package com.mowdowndevelopments.vault;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;

public class RepeatingTransactionsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_repeating_transactions);
		// Show the Up button in the action bar.
		setupActionBar();
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.repeating_transactions, menu);
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			Intent outIntent, sourceIntent = getIntent();
			boolean cameWithExtraID = sourceIntent.getBooleanExtra(TransactionCreationActivity.ARG_WITH_EXTRA_ID, false);
			try {
				outIntent = new Intent(this, Class.forName(sourceIntent.getStringExtra(SettingsActivity.ARG_SOURCE_ACTIVITY_NAME)));
			} catch (ClassNotFoundException e) {
				getActionBar().setHomeButtonEnabled(false);
				e.printStackTrace();
				return true;
			}
			if (cameWithExtraID){
				outIntent.putExtra(AccountDetailFragment.ARG_ACCOUNT_ID, sourceIntent.getLongExtra(AccountDetailFragment.ARG_ACCOUNT_ID, -1));
			}
			outIntent.putExtra(TransactionCreationActivity.ARG_WITH_EXTRA_ID, cameWithExtraID);
			NavUtils.navigateUpTo(this, outIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
