package com.mowdowndevelopments.vault;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SettingsFragment extends PreferenceFragment {

	public SettingsFragment() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see android.preference.PreferenceFragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		String header = getArguments().getString("settings");
		if (header.equalsIgnoreCase("security")){
			addPreferencesFromResource(R.xml.preferences_security);
		} else if (header.equalsIgnoreCase("ui")){
			addPreferencesFromResource(R.xml.preferences_display);
		} else if (header.equalsIgnoreCase("future")){
			addPreferencesFromResource(R.xml.preferences_repeat_transactions);
		} else if (header.equalsIgnoreCase("donate")){
			//TODO
		}
	}

	/* (non-Javadoc)
	 * @see android.preference.PreferenceFragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return super.onCreateView(inflater, container, savedInstanceState);
	}

}
