package com.mowdowndevelopments.vault;

import java.text.NumberFormat;
import java.text.ParseException;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;

public class NewAccountDialogFragment extends DialogFragment implements
		OnClickListener {

	public interface OnAccountCreated {
		public void openNewAccount(long accountID);
	}

	private EditText nameBox, initBalBox;
	private CheckBox defaultCheckBox;
	private Button cancelButton, createButton;
	private OnAccountCreated callback;
	private static OnAccountCreated dummyCallback = new OnAccountCreated() {

		@Override
		public void openNewAccount(long accountID) {
			// TODO Auto-generated method stub

		}

	};

	public NewAccountDialogFragment() {
		// Mandatory blank constructor.
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.dialog_fragment_new_account,
				container);
		nameBox = (EditText) v.findViewById(R.id.accountNameTextBox);
		initBalBox = (EditText) v.findViewById(R.id.startingBalTextBox);
		defaultCheckBox = (CheckBox) v.findViewById(R.id.defaultCheckBox);
		cancelButton = (Button) v.findViewById(R.id.cancelButton);
		createButton = (Button) v.findViewById(R.id.createButton);
		cancelButton.setOnClickListener(this);
		createButton.setOnClickListener(this);
		getDialog().setTitle(R.string.acctCreateDialog);
		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		getDialog().getWindow().setSoftInputMode(
				LayoutParams.SOFT_INPUT_STATE_VISIBLE);
	}

	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			callback = (OnAccountCreated) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnAccountCreated");
		}
	}

	public void onDetach() {
		super.onDetach();
		callback = dummyCallback;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cancelButton:
			dismiss();
			break;
		case R.id.createButton:
			try {
				NumberFormat formatter = NumberFormat.getInstance();
				formatter.setMaximumFractionDigits(2);
				long newEntryID = VaultDatabaseHelper.getInstance(
						getActivity()).createNewAccount(
						nameBox.getText().toString().trim(),
						formatter.parse(initBalBox.getText().toString().trim()).doubleValue(),
						defaultCheckBox.isChecked());
				callback.openNewAccount(newEntryID);
				dismiss();
			} catch (NumberFormatException e) {
				Log.e("NewAccount", "Invalid entry.", e);
				Toast.makeText(getActivity(), R.string.badTransactionValue, Toast.LENGTH_LONG).show();
			} catch (IllegalArgumentException e) {
				Log.e("NewAccount", "Invalid entry.", e);
				Toast.makeText(getActivity(), R.string.badTransactionValue, Toast.LENGTH_LONG).show();
			} catch (ParseException e) {
				Log.e("NewAccount", "Invalid entry.", e);
				Toast.makeText(getActivity(), R.string.badTransactionValue, Toast.LENGTH_LONG).show();
			}
			break;
		}

	}

}
