package com.mowdowndevelopments.vault;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.ArrayUtils;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.BadParcelableException;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CheckBox;
import android.widget.SearchView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.commonsware.cwac.loaderex.SQLiteCursorLoader;

/**
 * A fragment representing a single Account detail screen. This fragment is
 * either contained in a {@link AccountListActivity} in two-pane mode (on
 * tablets) or a {@link AccountDetailActivity} on handsets.
 * 
 * This will become a list fragment to hold all the transactions for an Account.
 */
public class AccountDetailFragment extends ListFragment implements
		MultiChoiceModeListener, LoaderCallbacks<Cursor>,
		OnItemLongClickListener, MenuItem.OnActionExpandListener,
		SearchView.OnQueryTextListener {
	private static final String STATE_FIRST_VISIBLE_POSITION = "first_visible_position";
	private TransactionListCursorAdapter adapter;
	private SQLiteCursorLoader loader;
	private long accountID;
	private ActionMode mActionMode;
	private Menu menu;
	private int searchArg;
	private AQuery aq;
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ACCOUNT_ID = "com.mowdowndevelopments.vault.account_id",
			ARG_TRANS_ID = "com.mowdowndevelopments.vault.transaction_id";

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public AccountDetailFragment() {
	}

	public interface Callbacks extends MainScreensCallbacksBase {
		public void onSearchQuerySubmitted(String query, int searchMode);
	}

	private Callbacks mCallbacks = sDummyCallbacks;
	private static Callbacks sDummyCallbacks = new Callbacks() {

		@Override
		public void setIfActionModeIsActive(boolean isActionModeActive) {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean isActionModeActive() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isDualPane() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void onSearchQuerySubmitted(String query, int searchMode) {
			// TODO Auto-generated method stub

		}

	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle arguments = getArguments();
		if (arguments != null) {
			accountID = arguments.getLong(ARG_ACCOUNT_ID);
		} else if (savedInstanceState != null) {
			accountID = savedInstanceState.getLong(ARG_ACCOUNT_ID);
		} else {
			throw new BadParcelableException(
					"No accountID argument could be found for some reason.");
		}
		adapter = new TransactionListCursorAdapter(getActivity(), null);
		setHasOptionsMenu(true);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		aq.id(R.id.postValue).text(
				NumberFormat.getCurrencyInstance().format(
						VaultDatabaseHelper.getInstance(
								getActivity().getApplication())
								.getPostedBalance(accountID)));
		aq.id(R.id.balValue).text(
				NumberFormat.getCurrencyInstance().format(
						VaultDatabaseHelper.getInstance(
								getActivity().getApplication())
								.getCurrentBalance(accountID)));
		if (mCallbacks.isDualPane()) {
			aq.id(R.id.accountLabel).text(
					VaultDatabaseHelper.getInstance(
							getActivity().getApplication()).getAccountName(
							accountID));
		} else {
			aq.id(R.id.accountLabel).invisible();
		}
		if (savedInstanceState != null) {
			if (savedInstanceState.getLong(ARG_ACCOUNT_ID) == accountID
					&& savedInstanceState
							.containsKey(STATE_FIRST_VISIBLE_POSITION)) {
				getListView()
						.smoothScrollToPosition(
								savedInstanceState
										.getInt(STATE_FIRST_VISIBLE_POSITION));
			}
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.fragment_transaction_list, menu);
		this.menu = menu;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case (R.id.newTransMenuItem):
			createNewTransaction();
			return true;
		case (R.id.tagSearch):
			searchArg = SearchResultsListFragment.searchArgTag;
			setActionBarButtonsVisibility(false);
			item.setOnActionExpandListener(this);
			return false;
		case (R.id.nameSearch):
			searchArg = SearchResultsListFragment.searchArgName;
			setActionBarButtonsVisibility(false);
			item.setOnActionExpandListener(this);
			return false;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_transaction_list,
				container, false);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setListAdapter(adapter);
		aq = new AQuery(getActivity(), view);
		if (!mCallbacks.isDualPane()) {
			ActionBar actionBar = getActivity().getActionBar();
			actionBar.setTitle(R.string.transString);
			actionBar.setSubtitle(VaultDatabaseHelper.getInstance(
					getActivity().getApplication()).getAccountName(accountID));
		}
		getListView().setVisibility(View.INVISIBLE);
		aq.id(R.id.loadingTime).visible();
		getListView().setMultiChoiceModeListener(this);
		LoaderManager.enableDebugLogging(true);
		getLoaderManager().initLoader(R.id.TransactionListLoader, null, this);
		// TODO: Restore current transaction list position.
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException(
					"Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		// Reset the active callbacks interface to the dummy implementation.
		mCallbacks = sDummyCallbacks;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_FIRST_VISIBLE_POSITION, getListView()
				.getFirstVisiblePosition());
		outState.putLong(ARG_ACCOUNT_ID, accountID);
	}

	protected void onCheckboxClicked(View v) {
		long taggedID = (Long) v.getTag();
		boolean isChecked = ((CheckBox) v).isChecked();
		VaultDatabaseHelper.getInstance(getActivity().getApplication())
				.setIfTransactionChecked(taggedID, isChecked);
		aq.id(R.id.postValue).text(
				NumberFormat.getCurrencyInstance().format(
						VaultDatabaseHelper.getInstance(getActivity())
								.getPostedBalance(accountID)));
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		StringBuilder builder = new StringBuilder()
				.append(TransactionListCursorAdapter.LOADEREX_INIT_QUERY);
		String sortPref = PreferenceManager.getDefaultSharedPreferences(
				getActivity()).getString("sortPref", "default");
		if (!sortPref.equalsIgnoreCase("default")) {
			builder.append(sortPref);
		}
		loader = new SQLiteCursorLoader(
				this.getActivity(),
				VaultDatabaseHelper.getInstance(getActivity().getApplication()),
				builder.toString(), new String[] { Long.toString(accountID) });
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> l, Cursor c) {
		adapter.changeCursor(c);
		aq.id(R.id.loadingTime).gone();
		if (getListView().getCount() == 0) {
			if (isResumed()) {
				aq.id(R.id.noTrans).visible().animate(android.R.anim.fade_in);
			} else {
				aq.id(R.id.noTrans).visible();
			}
		} else if (isResumed()) {
			aq.id(android.R.id.list).visible()
					.animate(android.R.anim.slide_in_left);
		} else {
			getListView().setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> l) {
		adapter.changeCursor(null);
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		Long[] ids = ArrayUtils.toObject(getListView().getCheckedItemIds());
		switch (item.getItemId()) {
		case R.id.editTransaction:
			editTransaction(ids[0]);
			return true;
		case R.id.deleteTrans:
			Log.d("ActionModeIDs", Arrays.toString(ids));
			return false;
		case R.id.confirmDeleteTrans:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.deleteTransaction(ids[0]);
				restartLocalLoader();
			} else {
				new BulkTransactionEraser(getActivity()).execute(ids);
			}
			mode.finish();
			return true;
		case R.id.repeatTransMonth:
			GregorianCalendar gCal = new GregorianCalendar();
			gCal.roll(Calendar.MONTH, true);
			long delay = gCal.getTimeInMillis()
					- Calendar.getInstance().getTimeInMillis();
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.replicateTransaction(ids[0], delay);
				restartLocalLoader();
			} else {
				new BulkReplicator(getActivity(), delay).execute(ids);
			}
			mode.finish();
			return true;
		case R.id.repeatTransNow:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.replicateTransaction(ids[0], 0);
				restartLocalLoader();
			} else {
				new BulkReplicator(getActivity(), 0).execute(ids);
			}
			mode.finish();
			return true;
		case R.id.repeatTransOneWeek:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.replicateTransaction(ids[0], 604800000);
				restartLocalLoader();
			} else {
				new BulkReplicator(getActivity(), 604800000).execute(ids);
			}
			mode.finish();
			return true;
		case R.id.repeatTransTomorrow:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.replicateTransaction(ids[0], 86400000);
				restartLocalLoader();
			} else {
				new BulkReplicator(getActivity(), 86400000).execute(ids);
			}
			mode.finish();
			return true;
		case R.id.repeatTransTwoWeek:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.replicateTransaction(ids[0], 1209600000);
				restartLocalLoader();
			} else {
				new BulkReplicator(getActivity(), 1209600000).execute(ids);
			}
			mode.finish();
			return true;
		case R.id.repeatTransYear:
			if (ids.length == 1) {
				VaultDatabaseHelper.getInstance(getActivity())
						.replicateTransaction(ids[0], 31560000000l);
				restartLocalLoader();
			} else {
				new BulkReplicator(getActivity(), 31560000000l).execute(ids);
			}
			mode.finish();
			return true;
		default:
			return false;
		}
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		MenuInflater inflater = mode.getMenuInflater();
		inflater.inflate(R.menu.action_mode_transaction_list, menu);
		mCallbacks.setIfActionModeIsActive(true);
		return true;
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
		mActionMode = null;
		mCallbacks.setIfActionModeIsActive(false);
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		mode.setTitle(getActivity().getActionBar().getTitle());
		return true;
	}

	@Override
	public void onItemCheckedStateChanged(ActionMode mode, int position,
			long id, boolean checked) {
		if (getListView().getCheckedItemCount() == 1) {
			mode.getMenu().findItem(R.id.editTransaction).setVisible(true);
			mode.setSubtitle(R.string.CABTransSubSingular);
		} else {
			mode.getMenu().findItem(R.id.editTransaction).setVisible(false);
			mode.setSubtitle(new StringBuilder()
					.append(getListView().getCheckedItemCount()).append(" ")
					.append(getActivity().getString(R.string.CABTransSub))
					.toString());
		}

	}

	@Override
	public boolean onItemLongClick(AdapterView<?> listView, View v,
			int position, long id) {
		if (mActionMode != null || mCallbacks.isActionModeActive()) {
			return false;
		}
		mActionMode = getActivity().startActionMode(this);
		return true;
	}

	public void restartLocalLoader() {
		if (getListView().getCount() == 0) {
			aq.id(R.id.noTrans).gone();
			aq.id(R.id.loadingTime).visible();
		}
		getLoaderManager().restartLoader(87267, null, this);
		aq.id(R.id.postValue).text(
				NumberFormat.getCurrencyInstance().format(
						VaultDatabaseHelper.getInstance(
								getActivity().getApplication())
								.getPostedBalance(accountID)));
		aq.id(R.id.balValue).text(
				NumberFormat.getCurrencyInstance().format(
						VaultDatabaseHelper.getInstance(
								getActivity().getApplication())
								.getCurrentBalance(accountID)));
	}

	public void createNewTransaction() {
		Intent intent = new Intent(this.getActivity(),
				TransactionCreationActivity.class);
		intent.putExtra(AccountListActivity.ARG_DUAL_PANE,
				mCallbacks.isDualPane()).putExtra(ARG_ACCOUNT_ID, accountID);
		startActivity(intent);
	}

	public void editTransaction(long transID) {
		Intent intent = new Intent(this.getActivity(),
				TransactionCreationActivity.class);
		intent.putExtra(AccountListActivity.ARG_DUAL_PANE,
				mCallbacks.isDualPane()).putExtra(ARG_ACCOUNT_ID, accountID)
				.putExtra(ARG_TRANS_ID, transID);
		startActivity(intent);
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		if (!(query.contains(";") || query.contains(")")
				|| query.contains("--") || query.contains("\""))) {
			mCallbacks.onSearchQuerySubmitted(query, searchArg);
		} else {
			Toast.makeText(getActivity(), R.string.noSpecialCharacters, Toast.LENGTH_LONG).show();
		}
		return true;
	}

	private void setActionBarButtonsVisibility(boolean isVisible) {
		menu.findItem(R.id.newTransMenuItem).setVisible(isVisible);
		menu.findItem(R.id.searchMenu).setVisible(isVisible);
		// menu.findItem(R.id.sortItem).setVisible(isVisible);
	}

	@Override
	public boolean onMenuItemActionCollapse(MenuItem item) {
		setActionBarButtonsVisibility(true);
		return true;
	}

	@Override
	public boolean onMenuItemActionExpand(MenuItem item) {
		SearchView searchView = (SearchView) item.getActionView();
		searchView.setOnQueryTextListener(this);
		searchView.setSubmitButtonEnabled(true);
		return true;
	}

	public class BulkTransactionEraser extends BulkEraser {

		public BulkTransactionEraser(Context c) {
			super(c, false);
			// TODO Auto-generated constructor stub
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			restartLocalLoader();
		}

	}

	public class BulkReplicator extends AsyncTask<Long, Integer, Void> {

		Context cxt;
		long delay;
		ProgressDialog dialog;

		public BulkReplicator(Context c, long delay) {
			cxt = c;
			this.delay = delay;
		}

		@Override
		protected Void doInBackground(Long... params) {
			int length = params.length;
			for (int i = 0; i < length; i++) {
				VaultDatabaseHelper.getInstance(cxt).replicateTransaction(
						params[i], delay);
				publishProgress((int) ((i / (float) length) * 100));
				if (isCancelled()) {
					break;
				}
			}
			dialog.dismiss();
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onProgressUpdate(Progress[])
		 */
		@Override
		protected void onProgressUpdate(Integer... values) {
			if (dialog == null) {
				dialog = ProgressDialog.show(cxt, "",
						cxt.getString(R.string.replicate), false, false);
			}
			dialog.setProgress(values[0]);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			restartLocalLoader();
		}

	}
}
