package com.mowdowndevelopments.vault;

import android.app.IntentService;
import android.content.Intent;

public class TagBinderService extends IntentService {
	static final String ARG_TAGS = "tagArray";

	public TagBinderService() {
		super("Tag_Binder_Service");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		long transactionID = intent.getLongExtra(AccountDetailFragment.ARG_TRANS_ID, -1);
		String[] tags = intent.getStringArrayExtra(ARG_TAGS);
		if (transactionID != -1 && tags != null) {
			VaultDatabaseHelper.getInstance(this).associateTagsWithTransaction(
					tags, transactionID);
		}
	}
}
