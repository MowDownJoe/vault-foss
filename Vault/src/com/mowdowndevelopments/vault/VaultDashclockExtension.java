package com.mowdowndevelopments.vault;

import android.content.Intent;

import com.google.android.apps.dashclock.api.DashClockExtension;
import com.google.android.apps.dashclock.api.ExtensionData;

public class VaultDashclockExtension extends DashClockExtension {

	public VaultDashclockExtension() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onUpdateData(int arg0) {
		long accountID = getSharedPreferences(
				DashClockExtensionSetupActivity.ARG_PREFS_DASHCLOCK, 0)
				.getLong(
						DashClockExtensionSetupActivity.ARG_DASHCLOCK_TO_DISPLAY,
						-1);
		Intent intent = new Intent(this, PINGuardActivity.class).putExtra(
				AccountDetailFragment.ARG_ACCOUNT_ID, accountID).putExtra(
				TransactionCreationActivity.ARG_WITH_EXTRA_ID, true);
		if (accountID != -1) {
			publishUpdate(new ExtensionData()
					.visible(true)
					.icon(R.drawable.ic_stat_notify)
					.clickIntent(intent)
					.status(VaultDatabaseHelper.getInstance(this)
							.createDashclockExtensionStatus(accountID))
					.expandedTitle(
							VaultDatabaseHelper.getInstance(this)
									.createDashclockExtensionExpandedTitle(
											accountID))
					.expandedBody(
							VaultDatabaseHelper.getInstance(this)
									.createDashclockExtensionExpandedBody(
											accountID)));
		} else {
			publishUpdate(new ExtensionData().visible(false));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.android.apps.dashclock.api.DashClockExtension#onInitialize
	 * (boolean)
	 */
	@Override
	protected void onInitialize(boolean isReconnect) {
		super.onInitialize(isReconnect);
		setUpdateWhenScreenOn(true);
	}

}
