package com.mowdowndevelopments.vault;

import android.content.AsyncTaskLoader;
import android.content.Context;

public class TagLoader extends AsyncTaskLoader<String[]> {

	private Long transactionID = Long.valueOf(-1);

	public TagLoader(Context c) {
		super(c);
	}

	public TagLoader(Context c, long transactionID) {
		super(c);
		this.transactionID = transactionID;
	}

	@Override
	public String[] loadInBackground() {
		if (transactionID == -1) {
			return VaultDatabaseHelper.getInstance(getContext())
					.getTagsAsArray();
		} else {
			return VaultDatabaseHelper.getInstance(getContext())
					.getTagsForTransaction(transactionID);
		}
	}

}
