package com.mowdowndevelopments.vault;

import java.util.Arrays;
import java.util.LinkedList;

import org.apache.commons.lang3.ArrayUtils;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class TagListFragment extends ListFragment implements
		MultiChoiceModeListener {

	private ArrayAdapter<String> adapter;
	private LinkedList<String> list;
	private long transactionID;

	public TagListFragment() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Fragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		list = new LinkedList<String>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Fragment#onActivityCreated(android.os.Bundle)
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		transactionID = getActivity().getIntent().getLongExtra(
				AccountDetailFragment.ARG_TRANS_ID, -1);
		adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_activated_1,
				android.R.id.text1, this.list);
		ListView list = getListView();
		list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		list.setMultiChoiceModeListener(this);
		setListAdapter(adapter);
		if (transactionID != -1) {
			setListShown(false);
			new TagListBuilder(getActivity(), transactionID).execute();
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.fragment_tag_list, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case (R.id.addTagItem):
			TagEntryDialogFragment dialog = new TagEntryDialogFragment();
			dialog.show(getActivity().getFragmentManager(), "TagEntryDialog");
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		Long[] ids = ArrayUtils.toObject(getListView().getCheckedItemIds());
		switch (item.getItemId()) {
		case R.id.deleteTag:
			if (ids.length == 1) {
				list.remove(ids[0].intValue());
			} else {
				int length = ids.length;
				for (int i = 0; i < length; i++) {
					list.remove(ids[i].intValue());
				}
			}
			adapter.notifyDataSetChanged();
			return true;
		case R.id.editTag:
			return true;
		default:
			return false;
		}
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		MenuInflater inflater = mode.getMenuInflater();
		inflater.inflate(R.menu.action_mode_tag_list, menu);
		return true;
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	}

	@Override
	public void onItemCheckedStateChanged(ActionMode mode, int position,
			long id, boolean checked) {
		if (getListView().getCheckedItemCount() == 1) {
			mode.getMenu().findItem(R.id.editTag).setVisible(true);
			mode.setSubtitle("");
		} else {
			mode.getMenu().findItem(R.id.editTag).setVisible(false);
			mode.setSubtitle("");
		}
	}

	public class TagListBuilder extends TagListBuilderBase {

		public TagListBuilder(Context context, Long id) {
			super(context, id);
		}

		public TagListBuilder(Context context, long id) {
			super(context, Long.valueOf(id));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (!isRemoving() || !isDetached() || !getActivity().isFinishing()) {
				list.addAll(Arrays.asList(tags));
				adapter.notifyDataSetChanged();
				if (isResumed()) {
					setListShown(true);
				} else {
					setListShownNoAnimation(true);
				}
				Log.d("Stable IDs test",
						Boolean.toString(adapter.hasStableIds()));
			}
		}

	}

	public void addTag(String tag) {
		list.add(tag);
		adapter.notifyDataSetChanged();
	}

	protected void associateTagsWithTransaction(long transactionID)
			throws IllegalArgumentException {
		if (list.size() != 0) {
			String[] tags = list.toArray(new String[list.size()]);
			Intent intent = new Intent(getActivity(), TagBinderService.class)
					.putExtra(TagBinderService.ARG_TAGS, tags).putExtra(
							AccountDetailFragment.ARG_TRANS_ID, transactionID);
			getActivity().startService(intent);
		}
	}

}
