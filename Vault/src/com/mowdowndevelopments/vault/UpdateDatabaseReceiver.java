package com.mowdowndevelopments.vault;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class UpdateDatabaseReceiver extends BroadcastReceiver {
	public UpdateDatabaseReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (VaultDatabaseHelper.getInstance(context).getRepeatingTransactions() != null) {
			context.startService(new Intent(context,
					DatabaseUpdatingService.class));
		}
	}

	
}
